<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends App_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("profile_model");   
    }

    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        
        // if(!$this->ion_auth_acl->has_permission('master-profile-update')){
        //     $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
        //     redirect('dashboard', 'refresh');
        // }

        if($this->ion_auth->in_group('6')){
            $this->profil_mahasiswa();
        } else if ($this->ion_auth->in_group('5')){
            $this->profil_dosen();
        }
    }

    public function edit_dosen()
    {
        if (!empty($this->input->post())){
            $data = $this->input->post(); 
        } else {
            $nip = $this->ion_auth->user()->row()->username;
            $data = $this->profile_model->get_data_dosen_by_id($nip);
        }

        $form = $this->_populate_form_dosen($data);

        $data = array(
            'form'          => $form,
            'title'         => 'Edit Profile',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Edit Profile'),
            'url_form'      => base_url('profile/edit_dosen')
        );

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $nip = $this->ion_auth->user()->row()->username;
            $this->profile_model->update_dosen($nip, $post);
            $this->session->set_flashdata('success', 'Data telah berhasil dirubah.');
            redirect('profile', 'refresh');
        } else {
            $this->layout->view('profile/form_profile', $data);
        }
    }

    public function profil_dosen()
    {
        if (!empty($this->input->post())){
            $data = $this->input->post(); 
        } else {
            $nip = $this->ion_auth->user()->row()->username;
            $data = $this->profile_model->get_data_dosen_by_id($nip);
        }

        $form = $this->_populate_view_dosen($data);

        $data = array(
            'form'          => $form,
            'title'         => 'Profile',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Profile')
        );

            $this->layout->view('profile/profil_dosen', $data);

    }

    public function profil_mahasiswa()
    {
        if (!empty($this->input->post())){
            $data = $this->input->post(); 
        } else {
            $nrp = $this->ion_auth->user()->row()->username;
            $data = $this->profile_model->get_data_mahasiswa_by_id($nrp);
        }

        $form = $this->_populate_view_mahasiswa($data);

        $data = array(
            'form'          => $form,
            'title'         => 'Profile',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Profile')
        );

        
            $this->layout->view('profile/profil_mahasiswa', $data);

    }

    public function edit_mahasiswa()
    {
        if (!empty($this->input->post())){
            $data = $this->input->post(); 
        } else {
            $nrp = $this->ion_auth->user()->row()->username;
            $data = $this->profile_model->get_data_mahasiswa_by_id($nrp);
        }

        $form = $this->_populate_form_mahasiswa($data);

        $data = array(
            'form'          => $form,
            'title'         => 'Edit Profile',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Edit Profile'),
            'url_form'      => base_url('profile/edit_mahasiswa')
        );

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $nrp = $this->ion_auth->user()->row()->username;
            $this->profile_model->update_mahasiswa($nrp, $post);
            $this->session->set_flashdata('success', 'Data telah berhasil dirubah.');
            redirect('profile', 'refresh');
        } else {
            $this->layout->view('profile/form_mahasiswa', $data);
        }
    }


    public function _populate_form_dosen($data)
    {
        $req_sign = ' <span class="text-danger"> * </span>';
        $form = array(
            'nip' => array(
                'label' => 'NIP',
                'input' => array(
                    'name'          => 'nip',
                    'id'            => 'nip',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan NIP'
                )
            ),
            'nama' => array(
                'label'  => 'Nama'.$req_sign,
                'input' => array(
                    'name'          => 'nama',
                    'id'            => 'nama',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Nama'
                )
            ),
            'alamat' => array(
                'label'  => 'Alamat',
                'input' => array(
                    'name'          => 'alamat',
                    'id'            => 'alamat',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Alamat'
                )
            ),
            'hp' => array(
                'label'  => 'No. HP'.$req_sign,
                'input' => array(
                    'name'          => 'hp',
                    'id'            => 'hp',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan HP'
                )
            ),
            'jk' => array(
                'label'  => 'Jenis Kelamin',
                'input' => array(
                    'name'          => 'jk',
                    'id'            => 'jk',
                    'class'         => 'form-control app-select2',
                    'options'        => array(
                        'Pilih Jenis Kelamin',
                        'Laki-Laki' => 'Laki-Laki',
                        'Perempuan' => 'Perempuan',
                    )
                )
            ),
            'tempat_lahir' => array(
                'label'  => 'Tempat Lahir',
                'input' => array(
                    'name'          => 'tempat_lahir',
                    'id'            => 'tempat_lahir',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Tempat Lahir'
                )
            ),
            'tgl_lahir' => array(
                'label'  => 'Tanggal Lahir',
                'input' => array(
                    'name'          => 'tgl_lahir',
                    'id'            => 'tgl_lahir',
                    'class'         => 'form-control app-datepicker',
                    'type'          => 'date',
                    'placeholder'   => 'Masukkan Tanggal Lahir'
                )
            ),
            'email' => array(
                'label'  => 'E-mail'.$req_sign,
                'input' => array(
                    'name'          => 'email',
                    'id'            => 'email',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Email'
                )
            ),
        );

        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('hp', 'HP', 'required');
        $this->form_validation->set_rules('email', 'E-mail', 'required');

        foreach ($data as $key => $value) {
            $form_select = array ('jk');
            if (in_array($key, $form_select)){
                $form[$key]['input']['selected'] = $value;
            }
            else {
               $form[$key]['input']['value'] = $value;
            }
        }
        
        return $form;
    }

    public function _populate_view_dosen($data)
    {
        $req_sign = ' <span class="text-danger"> * </span>';
        $form = array(
            'nip' => array(
                'label' => 'NIP',
                'input' => array(
                    'name'          => 'nip',
                    'id'            => 'nip',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan NIP',
                    'readonly'      => 'true',
                )
            ),
            'nama' => array(
                'label'  => 'Nama',
                'input' => array(
                    'name'          => 'nama',
                    'id'            => 'nama',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Nama',
                    'readonly'      => 'true',
                )
            ),
            'alamat' => array(
                'label'  => 'Alamat',
                'input' => array(
                    'name'          => 'alamat',
                    'id'            => 'alamat',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Alamat',
                    'readonly'      => 'true',
                )
            ),
            'hp' => array(
                'label'  => 'No. HP',
                'input' => array(
                    'name'          => 'hp',
                    'id'            => 'hp',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan HP',
                    'readonly'      => 'true',
                )
            ),
            'jk' => array(
                'label'  => 'Jenis Kelamin',
                'input' => array(
                    'name'          => 'jk',
                    'id'            => 'jk',
                    'class'         => 'form-control app-select2',
                    'options'        => array(
                        'Pilih Jenis Kelamin',
                        'Laki-Laki' => 'Laki-Laki',
                        'Perempuan' => 'Perempuan',
                    )
                )
            ),
            'tempat_lahir' => array(
                'label'  => 'Tempat Lahir',
                'input' => array(
                    'name'          => 'tempat_lahir',
                    'id'            => 'tempat_lahir',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Tempat Lahir',
                    'readonly'      => 'true',
                )
            ),
            'tgl_lahir' => array(
                'label'  => 'Tanggal Lahir',
                'input' => array(
                    'name'          => 'tgl_lahir',
                    'id'            => 'tgl_lahir',
                    'class'         => 'form-control app-datepicker',
                    'type'          => 'date',
                    'placeholder'   => 'Masukkan Tanggal Lahir',
                    'readonly'      => 'true',
                )
            ),
            'email' => array(
                'label'  => 'E-mail'.$req_sign,
                'input' => array(
                    'name'          => 'email',
                    'id'            => 'email',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Email',
                    'readonly'      => 'true',
                )
            ),
        );

        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('hp', 'HP', 'required');
        $this->form_validation->set_rules('email', 'E-mail', 'required');

        foreach ($data as $key => $value) {
            $form_select = array ('jk');
            if (in_array($key, $form_select)){
                $form[$key]['input']['selected'] = $value;
            }
            else {
               $form[$key]['input']['value'] = $value;
            }
        }
        
        return $form;
    }

    public function _populate_form_mahasiswa($data)
    {
        $req_sign = ' <span class="text-danger"> * </span>';
        $form = array(
            'nrp' => array(
                'label' => 'NRP'.$req_sign,
                'input' => array(
                    'name'          => 'nrp',
                    'id'            => 'nrp',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan NRP',
                    'readonly'      => 'true',
                )
            ),
            'nama' => array(
                'label'  => 'Nama'.$req_sign,
                'input' => array(
                    'name'          => 'nama',
                    'id'            => 'nama',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Nama'
                )
            ),
            'alamat' => array(
                'label'  => 'Alamat'.$req_sign,
                'input' => array(
                    'name'          => 'alamat',
                    'id'            => 'alamat',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Alamat'
                )
            ),
            'hp' => array(
                'label'  => 'No. HP'.$req_sign,
                'input' => array(
                    'name'          => 'hp',
                    'id'            => 'hp',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan HP'
                )
            ),
            'jk' => array(
                'label'  => 'Jenis Kelamin'.$req_sign,
                'input' => array(
                    'name'          => 'jk',
                    'id'            => 'jk',
                    'class'         => 'form-control app-select2',
                    'options'        => array(
                        'Pilih Jenis Kelamin',
                        'Laki-Laki' => 'Laki-Laki',
                        'Perempuan' => 'Perempuan',
                    )
                )
            ),
            'tempat_lahir' => array(
                'label'  => 'Tempat Lahir'.$req_sign,
                'input' => array(
                    'name'          => 'tempat_lahir',
                    'id'            => 'tempat_lahir',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Tempat Lahir'
                )
            ),
            'tgl_lahir' => array(
                'label'  => 'Tanggal Lahir'.$req_sign,
                'input' => array(
                    'name'          => 'tgl_lahir',
                    'id'            => 'tgl_lahir',
                    'class'         => 'form-control app-datepicker',
                    'placeholder'   => 'Masukkan Tanggal Lahir'
                )
            ),
            'email' => array(
                'label'  => 'E-mail'.$req_sign,
                'input' => array(
                    'name'          => 'email',
                    'id'            => 'email',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Email'
                )
            ),
            'asal_sekolah' => array(
                'label'  => 'Asal Sekolah'.$req_sign,
                'input' => array(
                    'name'          => 'asal_sekolah',
                    'id'            => 'asal_sekolah',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Asal Sekolah'
                )
            ),
            'asal_kota' => array(
                'label'  => 'Asal Kota',
                'input' => array(
                    'name'          => 'asal_kota',
                    'id'            => 'asal_kota',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Asal Kota'
                )
            ),
            'nama_ortu' => array(
                'label'  => 'Nama Orang Tua',
                'input' => array(
                    'name'          => 'nama_ortu',
                    'id'            => 'nama_ortu',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Nama Orang Tua'
                )
            ),
            'alamat_ortu' => array(
                'label'  => 'Alamat Orang Tua',
                'input' => array(
                    'name'          => 'alamat_ortu',
                    'id'            => 'alamat_ortu',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Alamat Orang Tua'
                )
            ),
            'hp_ortu' => array(
                'label'  => 'No. HP Orang Tua',
                'input' => array(
                    'name'          => 'hp_ortu',
                    'id'            => 'hp_ortu',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan HP Orang Tua'
                )
            ),
        );

        $this->form_validation->set_rules('nrp', 'NRP', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('hp', 'No. HP', 'required');
        $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('email', 'E-mail', 'required');
        $this->form_validation->set_rules('asal_sekolah', 'Asal Sekolah', 'required');
        foreach ($data as $key => $value) {
            $form_select = array ('jk');
            if (in_array($key, $form_select)){
                $form[$key]['input']['selected'] = $value;
            }
            else {
               $form[$key]['input']['value'] = $value;
            }
        }

        return $form;
    }

    public function _populate_view_mahasiswa($data)
    {
        $req_sign = ' <span class="text-danger"> * </span>';
        $form = array(
            'nrp' => array(
                'label' => 'NRP',
                'input' => array(
                    'name'          => 'nrp',
                    'id'            => 'nrp',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan NRP',
                    'readonly'      => 'true',
                )
            ),
            'nama' => array(
                'label'  => 'Nama',
                'input' => array(
                    'name'          => 'nama',
                    'id'            => 'nama',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Nama',
                    'readonly'      => 'true',
                )
            ),
            'alamat' => array(
                'label'  => 'Alamat',
                'input' => array(
                    'name'          => 'alamat',
                    'id'            => 'alamat',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Alamat',
                    'readonly'      => 'true',
                )
            ),
            'hp' => array(
                'label'  => 'No. HP',
                'input' => array(
                    'name'          => 'hp',
                    'id'            => 'hp',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan HP',
                    'readonly'      => 'true',
                )
            ),
            'jk' => array(
                'label'  => 'Jenis Kelamin',
                'input' => array(
                    'name'          => 'jk',
                    'id'            => 'jk',
                    'class'         => 'form-control app-select2',
                    'options'        => array(
                        'Pilih Jenis Kelamin',
                        'Laki-Laki' => 'Laki-Laki',
                        'Perempuan' => 'Perempuan',
                    )
                )
            ),
            'tempat_lahir' => array(
                'label'  => 'Tempat Lahir',
                'input' => array(
                    'name'          => 'tempat_lahir',
                    'id'            => 'tempat_lahir',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Tempat Lahir',
                    'readonly'      => 'true',
                )
            ),
            'tgl_lahir' => array(
                'label'  => 'Tanggal Lahir',
                'input' => array(
                    'name'          => 'tgl_lahir',
                    'id'            => 'tgl_lahir',
                    'class'         => 'form-control app-datepicker',
                    'placeholder'   => 'Masukkan Tanggal Lahir',
                    'readonly'      => 'true',
                )
            ),
            'email' => array(
                'label'  => 'E-mail',
                'input' => array(
                    'name'          => 'email',
                    'id'            => 'email',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Email',
                    'readonly'      => 'true',
                )
            ),
            'asal_sekolah' => array(
                'label'  => 'Asal Sekolah',
                'input' => array(
                    'name'          => 'asal_sekolah',
                    'id'            => 'asal_sekolah',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Asal Sekolah',
                    'readonly'      => 'true',
                )
            ),
            'asal_kota' => array(
                'label'  => 'Asal Kota',
                'input' => array(
                    'name'          => 'asal_kota',
                    'id'            => 'asal_kota',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Asal Kota',
                    'readonly'      => 'true',
                )
            ),
            'nama_ortu' => array(
                'label'  => 'Nama Orang Tua',
                'input' => array(
                    'name'          => 'nama_ortu',
                    'id'            => 'nama_ortu',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Nama Orang Tua',
                    'readonly'      => 'true',
                )
            ),
            'alamat_ortu' => array(
                'label'  => 'Alamat Orang Tua',
                'input' => array(
                    'name'          => 'alamat_ortu',
                    'id'            => 'alamat_ortu',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Alamat Orang Tua',
                    'readonly'      => 'true',
                )
            ),
            'hp_ortu' => array(
                'label'  => 'No. HP Orang Tua',
                'input' => array(
                    'name'          => 'hp_ortu',
                    'id'            => 'hp_ortu',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan HP Orang Tua',
                    'readonly'      => 'true',
                )
            ),
        );

        $this->form_validation->set_rules('nrp', 'NRP', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('hp', 'No. HP', 'required');
        $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('email', 'E-mail', 'required');
        $this->form_validation->set_rules('asal_sekolah', 'Asal Sekolah', 'required');
        foreach ($data as $key => $value) {
            $form_select = array ('jk');
            if (in_array($key, $form_select)){
                $form[$key]['input']['selected'] = $value;
            }
            else {
               $form[$key]['input']['value'] = $value;
            }
        }

        return $form;
    }

}