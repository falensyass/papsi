<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transkrip extends App_Controller {


    public function __construct()
    {
        parent::__construct();
        $this->load->model('laporan/transkrip_model');
    }

    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Transkrip Nilai');

        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Transkrip Nilai');

        if($this->ion_auth->in_group('6')){
            redirect('laporan/transkrip/nilai_mahasiswa', 'refresh');
            $nrp = $this->ion_auth->user()->row()->username;
        } else {
            $data['list_data'] = $this->transkrip_model->get_kelas()->result();
            $this->layout->view('laporan/transkrip/data_kelas', $data);
        }
    }

    public function nilai_mahasiswa(){
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Data Nilai');

        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Nilai');
        $nrp = $this->ion_auth->user()->row()->username;
        $data['list_mhs']= $this->transkrip_model->get_data_mhs($nrp)->row_array();
        // var_dump($data['list_mhs']); exit();
        $list_nilai= $this->transkrip_model->get_nilai_mhs($nrp)->result_array();
        $ipk = 0;
        $sks = 0;
        foreach($list_nilai as $key => $value){
            $nilai[$key]['nama_dsn'] = $value['nama_dsn'];
            $nilai[$key]['nama_mk'] = $value['nama_mk'];
            $nilai[$key]['nilai_tugas'] = $value['nilai_tugas'];
            $nilai[$key]['nilai_uts'] = $value['nilai_uts'];
            $nilai[$key]['nilai_uas'] = $value['nilai_uas'];
            $nilai[$key]['sks'] = $value['sks'];
            $total = ((0.3 * $value['nilai_tugas']) + (0.7 * ((0.5 * $value['nilai_uts']) + (0.5 * $value['nilai_uas']))));
            if ($total > 85 ){
                $huruf = 'A';
                $nilai_angka = 4;
                }
                Else if ($total > 75 && $total <= 85) {
                $huruf = 'AB';
                $nilai_angka = 3.5;
                }
                Else if ($total > 65 && $total <= 75) {
                $huruf = 'B';
                $nilai_angka = 3;
                }
                Else if ($total > 60 && $total <= 65) {
                $huruf = 'BC';
                $nilai_angka = 2.5;
                }
                Else if ($total > 55 && $total <= 60) {
                $huruf = 'C';
                $nilai_angka = 2;
                }
                Else if ($total > 40 && $total <= 55) {
                $huruf = 'D';
                $nilai_angka = 1;
                }
                Else { 
                $huruf = 'E';
                $nilai_angka = 0;
                }
            $nilai[$key]['nilai_huruf'] = $huruf;
            $nilai[$key]['nilai_ip'] = $nilai_angka * $value['sks'];
            $ipk = $ipk + $nilai[$key]['nilai_ip'];
            $sks = $sks + $value['sks'];
        }
        
        if(!empty($nilai)){
            $data['list_data'] = $nilai;
        } else {
            $data['list_data'] = array();
        }

        $nilai_kp = $data['list_mhs']['nilai'];

        if ($nilai_kp > 85 ){
            $nilai_huruf = 'A';
            $angka = 4;
            }
            Else if ($nilai_kp > 75 && $nilai_kp <= 85) {
            $nilai_huruf = 'AB';
            $angka = 3.5;
            }
            Else if ($nilai_kp > 65 && $nilai_kp <= 75) {
            $nilai_huruf = 'B';
            $angka = 3;
            }
            Else if ($nilai_kp > 60 && $nilai_kp <= 65) {
            $nilai_huruf = 'BC';
            $angka = 2.5;
            }
            Else if ($nilai_kp > 55 && $nilai_kp <= 60) {
            $nilai_huruf = 'C';
            $angka = 2;
            }
            Else if ($nilai_kp > 40 && $nilai_kp <= 55) {
            $nilai_huruf = 'D';
            $angka = 1;
            }
            Else { 
            $nilai_huruf = 'E';
            $angka = 0;
            }

            $data['list_mhs']['nilai_huruf'] = $nilai_huruf;
            $data['list_mhs']['nilai_ip'] = $angka * 6;
            $ipk = $ipk + $data['list_mhs']['nilai_ip'];
            $sks = $sks + 6;

            $nilai_ipk = $ipk / $sks;
            $data['list_mhs']['ipk'] = $nilai_ipk;

            if($nilai_ipk == 4){
                $predikat = 'Istimewa';
            } else if($nilai_ipk >= 3.5 && $nilai_ipk < 4){
                $predikat = 'Baik Sekali';
            } else {
                $predikat = 'Kurang Sekali';
            }

            $data['list_mhs']['predikat'] = $predikat;
            

        $data['title'] = 'Transkrip';

        $this->load->library('pdf');
    
        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "laporan-transkrip-papsi.pdf";
        $this->pdf->load_view('laporan/transkrip/laporan_transkrip_mhs', $data);
    }

    public function jadwal($id){
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Transkrip Nilai');

        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Transkrip Nilai');
        $data['list_data'] = $this->transkrip_model->get_jadwal($id)->result();
        $this->layout->view('laporan/transkrip/data_jadwal', $data);
    }

    public function detail($id){
        $data = array(
            'title'         => 'Transkrip Nilai',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Transkrip Nilai'),
        );
            $list_nilai = $this->transkrip_model->get_data_nilai($id)->result_array();
            $list_mhs = $this->transkrip_model->get_mhs($id)->result_array();

            $length_mhs = count($list_mhs);
            $length_nilai = count($list_nilai);
            


            for ($i=0; $i < $length_mhs; $i++) {
                if($i < $length_nilai){
                    for ($j = 0; $j < $length_nilai; $j++){
                        $tugas = 0;
                        $uas = 0;
                        $uts = 0;
                        $ujian = 0;
                        $total = 0;
                        if($list_mhs[$i]['id'] == $list_nilai[$j]['id_mahasiswa']){
                            $nilai[$i]['id_nilai'] = $list_nilai[$j]['id'];
                             $nilai[$i]['id_mhs'] = $list_mhs[$i]['id'];
                             $nilai[$i]['nrp'] = $list_mhs[$i]['nrp'];
                             $nilai[$i]['nama'] = $list_mhs[$i]['nama'];
                             $nilai[$i]['tugas'] = $list_nilai[$j]['nilai_tugas'];
                             $nilai[$i]['uts'] = $list_nilai[$j]['nilai_uts'];
                             $nilai[$i]['uas'] = $list_nilai[$j]['nilai_uas'];
                             $tugas = (0.3*$list_nilai[$j]['nilai_tugas']);
                             $uts = (0.5*$list_nilai[$j]['nilai_uts']);
                             $uas = (0.5*$list_nilai[$j]['nilai_uas']);
                             $ujian = 0.7 * ($uts + $uas);
                             $total = $tugas + $ujian;
                             if ($total > 85 ){
                             $huruf = 'A';
                             }
                             Else if ($total > 75 && $total <= 85) {
                             $huruf = 'AB';
                             }
                             Else if ($total > 65 && $total <= 75) {
                             $huruf = 'B';
                             }
                             Else if ($total > 60 && $total <= 65) {
                             $huruf = 'BC';
                             }
                             Else if ($total > 55 && $total <= 60) {
                             $huruf = 'C';
                             }
                             Else if ($total > 40 && $total <= 55) {
                             $huruf = 'D';
                             }
                             Else { 
                             $huruf = 'E';
                             }
                             $nilai[$i]['total'] = $total;
                             $nilai[$i]['huruf'] = $huruf;
                        }
                    }
                }
                else{
                    $nilai[$i]['id_nilai'] = 0;
                     $nilai[$i]['id_mhs'] = $list_mhs[$i]['id'];
                     $nilai[$i]['nrp'] = $list_mhs[$i]['nrp'];
                     $nilai[$i]['nama'] = $list_mhs[$i]['nama'];
                     $nilai[$i]['tugas'] = ' ';
                     $nilai[$i]['uts'] = ' ';
                     $nilai[$i]['uas'] = ' ';
                     $nilai[$i]['total'] = ' ';
                     $nilai[$i]['huruf'] = ' ';
                }
            }

            if(!empty($nilai)){
                $data['list_data'] = $nilai;
            } else {
                $data['list_data'] = array();
            }
            $this->layout->view('laporan/transkrip/data_nilai', $data);
    }

    public function get_data()
    {
        echo $this->transkrip_model->get_data();
    }

    private function get_all_mhs(){
        $data = $this->transkrip_model->get_all_mhs()->result();
        
        foreach ($data as $key){
            $result[$key->id] = $key->nama;
        }
        return $result;
    }

    public function print($id){
        $list_nilai     = $this->transkrip_model->get_data_nilai($id)->result_array();
        $list_mhs       = $this->transkrip_model->get_mhs($id)->result_array();
        $data['list_matkul']   = $this->transkrip_model->get_matkul($id)->row_array();

        $length_mhs = count($list_mhs);
        $length_nilai = count($list_nilai);
        
        for ($i=0; $i < $length_mhs; $i++) {
            if($i < $length_nilai){
                for ($j = 0; $j < $length_nilai; $j++){
                    $tugas = 0;
                    $uas = 0;
                    $uts = 0;
                    $ujian = 0;
                    $total = 0;
                    if($list_mhs[$i]['id'] == $list_nilai[$j]['id_mahasiswa']){
                        $nilai[$i]['id_nilai'] = $list_nilai[$j]['id'];
                             $nilai[$i]['id_mhs'] = $list_mhs[$i]['id'];
                             $nilai[$i]['nrp'] = $list_mhs[$i]['nrp'];
                             $nilai[$i]['nama'] = $list_mhs[$i]['nama'];
                             $nilai[$i]['tugas'] = $list_nilai[$j]['nilai_tugas'];
                             $nilai[$i]['uts'] = $list_nilai[$j]['nilai_uts'];
                             $nilai[$i]['uas'] = $list_nilai[$j]['nilai_uas'];
                             $tugas = (0.3*$list_nilai[$j]['nilai_tugas']);
                             $uts = (0.5*$list_nilai[$j]['nilai_uts']);
                             $uas = (0.5*$list_nilai[$j]['nilai_uas']);
                             $ujian = 0.7 * ($uts + $uas);
                             $total = $tugas + $ujian;
                             if ($total > 85 ){
                             $huruf = 'A';
                             }
                             Else if ($total > 75 && $total <= 85) {
                             $huruf = 'AB';
                             }
                             Else if ($total > 65 && $total <= 75) {
                             $huruf = 'B';
                             }
                             Else if ($total > 60 && $total <= 65) {
                             $huruf = 'BC';
                             }
                             Else if ($total > 55 && $total <= 60) {
                             $huruf = 'C';
                             }
                             Else if ($total > 40 && $total <= 55) {
                             $huruf = 'D';
                             }
                             Else { 
                             $huruf = 'E';
                             }
                             $nilai[$i]['total'] = $total;
                             $nilai[$i]['huruf'] = $huruf;
                    }
                }
            }
            else{
                $nilai[$i]['id_nilai'] = 0;
                 $nilai[$i]['id_mhs'] = $list_mhs[$i]['id'];
                 $nilai[$i]['nrp'] = $list_mhs[$i]['nrp'];
                 $nilai[$i]['nama'] = $list_mhs[$i]['nama'];
                 $nilai[$i]['tugas'] = ' ';
                 $nilai[$i]['uts'] = ' ';
                 $nilai[$i]['uas'] = ' ';
                 $nilai[$i]['total'] = ' ';
                 $nilai[$i]['huruf'] = ' ';
            }
        }
        if(!empty($nilai)){
            $data['list_data'] = $nilai;
        } else {
            $data['list_data'] = array();
        }
        $data['title'] = 'Transkrip';
        


        $data['dataku'] = array(
            [
                "nama" => "Petani Kode",
                "url" => "http://petanikode.com"
            ],
            [
                "nama" => "Petani Kode",
                "url" => "http://petanikode.com"
            ],
        );

        // var_dump($data['list_data']);
        // echo '<br>';
        // var_dump($data['dataku']); exit();
    
        $this->load->library('pdf');
    
        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "laporan-transkrip.pdf";
        $this->pdf->load_view('laporan/transkrip/laporan_transkrip', $data);
    
    
    }

    




}