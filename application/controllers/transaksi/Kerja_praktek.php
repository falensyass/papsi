<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kerja_praktek extends App_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array("transaksi/kerja_praktek_model"));
        $this->load->library('form_validation');
        // if($this->users_model->isNotLogin()) redirect(site_url('login'));   
    }

    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Kerja Praktek');

        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Kerja Praktek', 'Kerja Praktek');
        
        $data['list_data'] = array();

        $data["kerja_praktek"]=$this->kerja_praktek_model->get_data_kp();
        $this->layout->view('transaksi/kerja_praktek/data_kerja_praktek', $data);
    }

    // public function get_data_dosen()
    // {
    //     if (!$this->input->is_ajax_request()) {
    //        exit('No direct script access allowed');
    //     }

    //     echo $this->dosen_model->get_data_dosen();
    // }

}