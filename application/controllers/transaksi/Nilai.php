<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends App_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array("transaksi/nilai_model"));  
    }

    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Nilai');

        if(!$this->ion_auth_acl->has_permission('transaksi-nilai-read')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Nilai');

        if($this->ion_auth->in_group('5')){
            $nip = $this->ion_auth->user()->row()->username;
            $data['list_data'] = $this->nilai_model->get_jadwal_dosen($nip)->result();
            $this->layout->view('transaksi/nilai/data_jadwal', $data);
        }
        else if($this->ion_auth->in_group('6')){
            redirect('transaksi/nilai/nilai_mahasiswa', 'refresh');
        }
        else{
            $data['list_data'] = $this->nilai_model->get_kelas()->result();
            $this->layout->view('transaksi/nilai/data_kelas', $data);   
        }

    }

    public function jadwal($id){
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Data Nilai');

        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Nilai');
        $data['list_data'] = $this->nilai_model->get_jadwal($id)->result();
        $this->layout->view('transaksi/nilai/data_jadwal', $data);
    }

    public function nilai_mahasiswa(){
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Data Nilai');

        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Nilai');
        $nrp = $this->ion_auth->user()->row()->username;
        $list_nilai= $this->nilai_model->get_nilai_mhs($nrp)->result_array();
        foreach($list_nilai as $key => $value){
            $nilai[$key]['nama_dsn'] = $value['nama_dsn'];
            $nilai[$key]['nama_mk'] = $value['nama_mk'];
            $nilai[$key]['nilai_tugas'] = $value['nilai_tugas'];
            $nilai[$key]['nilai_uts'] = $value['nilai_uts'];
            $nilai[$key]['nilai_uas'] = $value['nilai_uas'];
            $total = ((0.3 * $value['nilai_tugas']) + (0.7 * ((0.5 * $value['nilai_uts']) + (0.5 * $value['nilai_uas']))));
            if ($total > 85 ){
                $huruf = 'A';
                }
                Else if ($total > 75 && $total <= 85) {
                $huruf = 'AB';
                }
                Else if ($total > 65 && $total <= 75) {
                $huruf = 'B';
                }
                Else if ($total > 60 && $total <= 65) {
                $huruf = 'BC';
                }
                Else if ($total > 55 && $total <= 60) {
                $huruf = 'C';
                }
                Else if ($total > 40 && $total <= 55) {
                $huruf = 'D';
                }
                Else { 
                $huruf = 'E';
                }
            $nilai[$key]['nilai_huruf'] = $huruf;
        }
        if(!empty($nilai)){
            $data['list_data'] = $nilai;
        } else {
            $data['list_data'] = array();
        }
        
        $this->layout->view('transaksi/nilai/data_nilai_mhs', $data);
    }

    public function detail($id){
        $data = array(
            'title'         => 'Data Nilai',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Data Nilai'),
        );
            $list_nilai = $this->nilai_model->get_data_nilai($id)->result_array();
            $list_mhs = $this->nilai_model->get_mhs($id)->result_array();

            $length_mhs = count($list_mhs);
            $length_nilai = count($list_nilai);
            


            for ($i=0; $i < $length_mhs; $i++) {
                if($i < $length_nilai){
                    for ($j = 0; $j < $length_nilai; $j++){
                        $tugas = 0;
                        $uas = 0;
                        $uts = 0;
                        $ujian = 0;
                        $total = 0;
                        if($list_mhs[$i]['id'] == $list_nilai[$j]['id_mahasiswa']){
                            $nilai[$i]['id_jadwal'] = $list_nilai[$j]['id_jadwal'];
                            $nilai[$i]['id_nilai'] = $list_nilai[$j]['id'];
                             $nilai[$i]['id_mhs'] = $list_mhs[$i]['id'];
                             $nilai[$i]['nrp'] = $list_mhs[$i]['nrp'];
                             $nilai[$i]['nama'] = $list_mhs[$i]['nama'];
                             $nilai[$i]['tugas'] = $list_nilai[$j]['nilai_tugas'];
                             $nilai[$i]['uts'] = $list_nilai[$j]['nilai_uts'];
                             $nilai[$i]['uas'] = $list_nilai[$j]['nilai_uas'];
                             $tugas = (0.3*$list_nilai[$j]['nilai_tugas']);
                             $uts = (0.5*$list_nilai[$j]['nilai_uts']);
                             $uas = (0.5*$list_nilai[$j]['nilai_uas']);
                             $ujian = 0.7 * ($uts + $uas);
                             $total = $tugas + $ujian;
                             if ($total > 85 ){
                             $huruf = 'A';
                             }
                             Else if ($total > 75 && $total <= 85) {
                             $huruf = 'AB';
                             }
                             Else if ($total > 65 && $total <= 75) {
                             $huruf = 'B';
                             }
                             Else if ($total > 60 && $total <= 65) {
                             $huruf = 'BC';
                             }
                             Else if ($total > 55 && $total <= 60) {
                             $huruf = 'C';
                             }
                             Else if ($total > 40 && $total <= 55) {
                             $huruf = 'D';
                             }
                             Else { 
                             $huruf = 'E';
                             }
                             $nilai[$i]['total'] = $total;
                             $nilai[$i]['huruf'] = $huruf;
                        }
                    }
                }
                else{
                    $nilai[$i]['id_jadwal'] =0;
                    $nilai[$i]['id_nilai'] = 0;
                     $nilai[$i]['id_mhs'] = $list_mhs[$i]['id'];
                     $nilai[$i]['nrp'] = $list_mhs[$i]['nrp'];
                     $nilai[$i]['nama'] = $list_mhs[$i]['nama'];
                     $nilai[$i]['tugas'] = ' ';
                     $nilai[$i]['uts'] = ' ';
                     $nilai[$i]['uas'] = ' ';
                     $nilai[$i]['total'] = ' ';
                     $nilai[$i]['huruf'] = ' ';
                }
            }



            if(!empty($nilai)){
                $data['list_data'] = $nilai;
            } else {
                $data['list_data']  = array();
            }
            $data['id_jadwal'] = $id;
            $this->layout->view('transaksi/nilai/data_nilai', $data);
    }

    public function add($id)
    {
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Nilai');

        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Tambah Nilai', 'Nilai');

        $list_nilai = $this->nilai_model->get_data_nilai($id)->result_array();
        $list_mhs = $this->nilai_model->get_mhs($id)->result_array();

        $length_mhs = count($list_mhs);
        $length_nilai = count($list_nilai);

        for ($i=0; $i < $length_mhs; $i++) {
            if($i < $length_nilai){
                for ($j = 0; $j < $length_nilai; $j++){
                    if($list_mhs[$i]['id'] == $list_nilai[$j]['id_mahasiswa']){
                        $nilai[$i]['id_jadwal'] = $id;
                         $nilai[$i]['id_mhs'] = $list_mhs[$i]['id'];
                         $nilai[$i]['nrp'] = $list_mhs[$i]['nrp'];
                         $nilai[$i]['nama'] = $list_mhs[$i]['nama'];
                         $nilai[$i]['tugas'] = $list_nilai[$j]['nilai_tugas'];
                         $nilai[$i]['uts'] = $list_nilai[$j]['nilai_uts'];
                         $nilai[$i]['uas'] = $list_nilai[$j]['nilai_uas'];
                    }
                }
            }
            else{
                $nilai[$i]['id_jadwal'] =$id;
                 $nilai[$i]['id_mhs'] = $list_mhs[$i]['id'];
                 $nilai[$i]['nrp'] = $list_mhs[$i]['nrp'];
                 $nilai[$i]['nama'] = $list_mhs[$i]['nama'];
                 $nilai[$i]['tugas'] = null;
                 $nilai[$i]['uts'] = null;
                 $nilai[$i]['uas'] = null;
            }
        }
        if(!empty($nilai)){
            $data['list_data'] = $nilai;
        } else {
            $data['list_data'] = array();
        }
            $data['id_jadwal'] = $id;
           $this->layout->view('transaksi/nilai/form_nilai', $data);
    }

    public function insert($id_jadwal)
    {
        if(!empty($this->input->post())) { 
            $post = $this->input->post();
            $res = $this->nilai_model->insert_nilai($post);
            $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
            redirect('transaksi/nilai/detail/'.$id_jadwal, 'refresh');
        }
    } 

    public function get_data()
    {
        echo $this->nilai_model->get_data();
    }

    public function delete($id_jadwal, $id)
    {
        $this->nilai_model->delete($id);
        $this->session->set_flashdata('success', 'Data telah berhasil dihapus.');
        redirect('transaksi/nilai/detail/'.$id_jadwal, 'refresh');
    }

    public function edit($id_jadwal, $id)
    {
        if (!empty($this->input->post())){
            $data = $this->input->post(); 
        } else {
            $data = $this->nilai_model->get_data_by_id($id);
        }

        $form = $this->_populate_form($data);

        $data = array(
            'form'          => $form,
            'title'         => 'Edit Nilai',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Edit Nilai'),
            'url_form'      => base_url('transaksi/nilai/edit/'.$id_jadwal.'/'.$id)
        );

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $this->nilai_model->update($id, $post);
            $this->session->set_flashdata('success', 'Data telah berhasil dirubah.');
            redirect('transaksi/nilai/detail/'.$id_jadwal, 'refresh');
        } else {
            $this->layout->view('transaksi/nilai/edit_nilai', $data);
        }
    }

    private function get_all_mhs(){
        $data = $this->nilai_model->get_all_mhs()->result();
        
        foreach ($data as $key){
            $result[$key->id] = $key->nama;
        }
        return $result;
    }

    public function _populate_form($data)
    {
        $req_sign = ' <span class="text-danger"> * </span>';
        $form = array(
            'id_mhs' => array(
                'label'     => 'Nama Mahasiswa'.$req_sign,
                'name'      => 'id_mhs',
                'option'    => $this->get_all_mhs(),
                'selected'  => isset($data['id_mhs']) ? $data['id_mhs'] : '',
                'extra'     => array(
                    'id'            => 'id_mhs',
                    'class'         => 'form-control app-select2',
                )
            ),
            'nilai_tugas' => array(
                'label'  => 'Nilai Tugas',
                'input' => array(
                    'name'          => 'nilai_tugas',
                    'id'            => 'nilai_tugas',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Nilai Tugas',
                )
            ),
            'nilai_uts' => array(
                'label'  => 'Nilai UTS',
                'input' => array(
                    'name'          => 'nilai_uts',
                    'id'            => 'nilai_uts',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Nilai UTS',
                )
            ),
            'nilai_uas' => array(
                'label'  => 'Nilai UAS',
                'input' => array(
                    'name'          => 'nilai_uas',
                    'id'            => 'nilai_uas',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Nilai UAS',
                )
            ),
        );

        $this->form_validation->set_rules('id_mhs', 'Nama Mahasiswa', 'required');

        foreach ($data as $key => $value) {
               $form[$key]['input']['value'] = $value;
        }

        return $form;
    }

}
