<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kerjapraktik extends App_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("transaksi/kerja_praktik_model");   
    }

    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Kerja Praktik');

        if(!$this->ion_auth_acl->has_permission('transaksi-kerja_praktik-read')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Kerja Praktik', 'kerja_praktik');
        
        if($this->ion_auth->in_group('5')){
            $nip = $this->ion_auth->user()->row()->username;
            $data['list_data'] = $this->kerja_praktik_model->get_data_dosen($nip)->result();
            $data['list_mhs'] = $this->kerja_praktik_model->get_mahasiswa()->result();
        }
        else if($this->ion_auth->in_group('6')){
            $nrp = $this->ion_auth->user()->row()->username;
            $data['list_data'] = $this->kerja_praktik_model->get_data_mahasiswa($nrp)->result();
            $data['list_mhs'] = $this->kerja_praktik_model->get_mahasiswa()->result();
        }
        else {
            $data['list_data'] = $this->kerja_praktik_model->get_data()->result();
            $data['list_mhs'] = $this->kerja_praktik_model->get_mahasiswa()->result();
        }
        
        $this->layout->view('transaksi/kerja_praktik/data_kerja_praktik', $data);
    }

    public function add()
    {
        $form = $this->_populate_form($this->input->post());
        $data = array(
            'form'  => $form,
            'title' => 'Tambah Kerja Praktik',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Tambah Kerja Praktik')
        );

        if(!$this->ion_auth_acl->has_permission('transaksi-kerja_praktik-create')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $this->kerja_praktik_model->insert($post);
            $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
            redirect('transaksi/kerjapraktik', 'refresh');
        } else {
            $this->layout->view('transaksi/kerja_praktik/form_kerja_praktik', $data);
        }
    }

    public function edit($id)
    {
        if (!empty($this->input->post())){
            $data = $this->input->post(); 
        } else {
            $data = $this->kerja_praktik_model->get_data_by_id($id);
        }

        $form = $this->_populate_form($data);

        $data = array(
            'form'          => $form,
            'title'         => 'Edit Kerja Praktik',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Edit Kerja Praktik'),
            'url_form'      => base_url('transaksi/kerjapraktik/edit/'.$id)
        );

        if(!$this->ion_auth_acl->has_permission('transaksi-kerja_praktik-update')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $this->kerja_praktik_model->update($id, $post);
            $this->session->set_flashdata('success', 'Data telah berhasil dirubah.');
            redirect('transaksi/kerjapraktik', 'refresh');
        } else {
            $this->layout->view('transaksi/kerja_praktik/form_kerja_praktik', $data);
        }
    }

    public function delete($id)
    {
        $data = array('title' => 'Hapus Kerja Praktik');

        if(!$this->ion_auth_acl->has_permission('transaksi-kerja_praktik-delete')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        $this->kerja_praktik_model->delete($id);
        $this->session->set_flashdata('success', 'Data telah berhasil dihapus.');
        redirect('transaksi/kerjapraktik', 'refresh');
    }

    public function anggota($id)
    {
        $data = array(
            'title' => 'Tambah Anggota',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Tambah Anggota'),
            'mahasiswa'     => $this->kerja_praktik_model->get_all_mahasiswa()->result(),
            'id_kerja_praktik' => $id
        );

        if (!empty($this->input->post())){
            foreach ($this->input->post('id_mhs') as $key => $val){
                if ($val != ""){
                    $this->form_validation->set_rules("id_mhs[".$key."]", "Data Mahasiswa", "required|greater_than[0]");
                }
            }
        }
        

        if($this->form_validation->run()){
            $post = $this->input->post();
            $res = $this->kerja_praktik_model->insert_anggota($post, $id);
            $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
            redirect('transaksi/kerjapraktik/', 'refresh');
        } else {
            $this->layout->view('transaksi/kerja_praktik/form_anggota', $data);
        }
    }

    public function nilai($id)
    {

            $data = array(
                'title' => 'Tambah Nilai',
                'breadcrumbs'   => $this->layout->get_breadcrumbs('Tambah Nilai'),
                'mahasiswa'     => $this->kerja_praktik_model->get_all_anggota($id)->result(),
                'id_kerja_praktik' => $id
            );

             if (!empty($this->input->post())){
            foreach ($this->input->post('nilai') as $key => $val){
                if ($val != ""){
                    $this->form_validation->set_rules("nilai[".$key."]", "Nilai", "required");
                }
            }
        }
        if($this->form_validation->run()){
            $post = $this->input->post();
            $res = $this->kerja_praktik_model->insert_nilai($post);
            $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
            redirect('transaksi/kerjapraktik/', 'refresh');
        } else {
            $this->layout->view('transaksi/kerja_praktik/form_nilai', $data);

        }


    }



    private function get_all_dosen(){
        $data = $this->kerja_praktik_model->get_all_dosen()->result();
        
        foreach ($data as $key){
            $result[$key->id] = $key->nama;
        }
        return $result;
    }

    public function _populate_form($data)
    {
        $req_sign = ' <span class="text-danger"> * </span>';
        $form = array(
            'judul_TA' => array(
                'label' => 'Judul Tugas Akhir'.$req_sign,
                'input' => array(
                    'name'          => 'judul_TA',
                    'id'            => 'judul_TA',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Judul Tugas Akhir'
                )
            ),
            'perusahaan' => array(
                'label' => 'Nama Perusahaan'.$req_sign,
                'input' => array(
                    'name'          => 'perusahaan',
                    'id'            => 'perusahaan',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Nama Perusahaan'
                )
            ),
            'id_dosen_pembimbing' => array(
                'label'     => 'Nama Dosen Pembimbing'.$req_sign,
                'name'      => 'id_dosen_pembimbing',
                'option'    => $this->get_all_dosen(),
                'selected'  => isset($data['id_dosen_pembimbing']) ? $data['id_dosen_pembimbing'] : '',
                'extra'     => array(
                    'id'            => 'id_dosen_pembimbing',
                    'class'         => 'form-control app-select2',
                )
            ),
            'id_dosen_penguji' => array(
                'label'     => 'Nama Dosen Penguji',
                'name'      => 'id_dosen_penguji',
                'option'    => $this->get_all_dosen(),
                'selected'  => isset($data['id_dosen_penguji']) ? $data['id_dosen_penguji'] : '',
                'extra'     => array(
                    'id'            => 'id_dosen_penguji',
                    'class'         => 'form-control app-select2',
                )
            ),
        );

        $this->form_validation->set_rules('judul_TA', 'Judul Tugas Akhir', 'required');
        $this->form_validation->set_rules('perusahaan', 'Nama Perusahaan', 'required');
        $this->form_validation->set_rules('id_dosen_pembimbing', 'Nama Dosen Pembimbing', 'required');

        foreach ($data as $key => $value)
        {
            $form[$key]['input']['value'] = $value;
        }

        return $form;
    }

}