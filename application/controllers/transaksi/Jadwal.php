<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends App_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('transaksi/jadwal_model');
    }

    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Jadwal Kuliah');

        if(!$this->ion_auth_acl->has_permission('transaksi-jadwal-read')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Jadwal Kuliah');

        if($this->ion_auth->in_group('5')){
            $nip = $this->ion_auth->user()->row()->username;
            $data['list_data'] = $this->jadwal_model->get_data_dosen($nip)->result();
        } elseif($this->ion_auth->in_group('6')){
            $nrp = $this->ion_auth->user()->row()->username;
            $data['list_data'] = $this->jadwal_model->get_data_mahasiswa($nrp)->result();
        } else {
            $data['list_data'] = $this->jadwal_model->get_data()->result();
        }
        
        $this->layout->view('transaksi/jadwal/data_jadwal', $data);

    }

    public function get_data()
    {
        echo $this->jadwal_model->get_data();
    }

    public function add()
    {
        $form = $this->_populate_form($this->input->post());
        $data = array(
            'form'  => $form,
            'title' => 'Tambah Jadwal Kuliah',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Tambah Jadwal Kuliah')
        );

        if(!$this->ion_auth_acl->has_permission('transaksi-jadwal-create')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $this->jadwal_model->insert($post);
            $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
            redirect('transaksi/jadwal', 'refresh');
        } else {
            $this->layout->view('transaksi/jadwal/form_jadwal', $data);
        }
    }

    public function edit($id)
    {
        if (!empty($this->input->post())){
            $data = $this->input->post(); 
        } else {
            $data = $this->jadwal_model->get_data_by_id($id);
        }

        $form = $this->_populate_form($data);

        $data = array(
            'form'          => $form,
            'title'         => 'Edit Jadwal Kuliah',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Edit Jadwal Kuliah'),
            'url_form'      => base_url('transaksi/jadwal/edit/'.$id)
        );
        
        if(!$this->ion_auth_acl->has_permission('transaksi-jadwal-update')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $this->jadwal_model->update($id, $post);
            $this->session->set_flashdata('success', 'Data telah berhasil dirubah.');
            redirect('transaksi/jadwal', 'refresh');
        } else {
            $this->layout->view('transaksi/jadwal/form_jadwal', $data);
        }
    }

    public function delete($id)
    {
        $data = array('title' => 'Hapus Jadwal');

        if(!$this->ion_auth_acl->has_permission('transaksi-jadwal-delete')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        $this->jadwal_model->delete($id);
        $this->session->set_flashdata('success', 'Data telah berhasil dihapus.');
        redirect('transaksi/jadwal', 'refresh');
    }

    private function get_all_matkul(){
        $data = $this->jadwal_model->get_all_matkul()->result();
        
        foreach ($data as $key){
            $result[$key->id] = $key->nama;
        }
        return $result;
    }

    private function get_all_dosen(){
        $data = $this->jadwal_model->get_all_dosen()->result();
        
        foreach ($data as $key){
            $result[$key->id] = $key->nama;
        }
        return $result;
    }

    private function get_all_kelas(){
        $data = $this->jadwal_model->get_all_kelas()->result();
        
        foreach ($data as $key){
            $result[$key->id] = $key->kelas;
        }
        return $result;
    }

    public function _populate_form($data)
    {
        $req_sign = ' <span class="text-danger"> * </span>';
        $form = array(
            'id_matkul' => array(
                'label'     => 'Nama Mata Kuliah'.$req_sign,
                'name'      => 'id_matkul',
                'option'    => $this->get_all_matkul(),
                'selected'  => isset($data['id_matkul']) ? $data['id_matkul'] : '',
                'extra'     => array(
                    'id'            => 'id_matkul',
                    'class'         => 'form-control app-select2',
                )
            ),
            'hari' => array(
                'label'  => 'Hari'.$req_sign,
                'input' => array(
                    'name'          => 'hari',
                    'id'            => 'hari',
                    'class'         => 'form-control app-select2',
                    'options'        => array(
                        '-- Pilih Hari --',
                        'Senin'    => 'Senin',
                        'Selasa'    => 'Selasa',
                        'Rabu'    => 'Rabu',
                        'Kamis'    => 'Kamis',
                        'Jumat'    => 'Jumat',
                    )
                )
            ),
            'jam_masuk' => array(
                'label'  => 'Jam Masuk'.$req_sign,
                'input' => array(
                    'name'          => 'jam_masuk',
                    'id'            => 'jam_masuk',
                    'class'         => 'form-control app-timepicker',
                    'placeholder'   => 'Masukkan Jam Masuk'
                )
            ),
            'jam_pulang' => array(
                'label'  => 'Jam Pulang'.$req_sign,
                'input' => array(
                    'name'          => 'jam_pulang',
                    'id'            => 'jam_pulang',
                    'class'         => 'form-control app-timepicker',
                    'placeholder'   => 'Masukkan Jam Pulang'
                )
            ),
            'kelas' => array(
                'label'     => 'Kelas'.$req_sign,
                'name'      => 'kelas',
                'option'    => $this->get_all_kelas(),
                'selected'  => isset($data['kelas']) ? $data['kelas'] : '',
                'extra'     => array(
                    'id'            => 'kelas',
                    'class'         => 'form-control app-select2',
                )
            ),
            'id_dosen' => array(
                'label'     => 'Nama Dosen'.$req_sign,
                'name'      => 'id_dosen',
                'option'    => $this->get_all_dosen(),
                'selected'  => isset($data['id_dosen']) ? $data['id_dosen'] : '',
                'extra'     => array(
                    'id'            => 'id_dosen',
                    'class'         => 'form-control app-select2',
                )
            ),
        );

        $this->form_validation->set_rules('id_matkul', 'Nama Mata Kuliah', 'required');
        $this->form_validation->set_rules('hari', 'Hari', 'required');
        $this->form_validation->set_rules('jam_masuk', 'Jam Masuk', 'required');
        $this->form_validation->set_rules('jam_pulang', 'Jam Pulang', 'required');
        $this->form_validation->set_rules('kelas', 'Kelas', 'required');
        $this->form_validation->set_rules('id_dosen', 'Nama Dosen', 'required');

        foreach ($data as $key => $value) {
            $form_select = array ('hari', 'kelas');
            if (in_array($key, $form_select)){
                $form[$key]['input']['selected'] = $value;
            }
            else {
               $form[$key]['input']['value'] = $value;
            }
        }

        return $form;
    }

}
