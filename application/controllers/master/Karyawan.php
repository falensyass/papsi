<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends App_Controller {

    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Karyawan');

        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Karyawan', 'Karyawan');
        
        $data['list_data'] = array();
        
        $this->layout->view('master/karyawan/data_karyawan', $data);
    }

    public function get_data_karyawan()
    {
        if (!$this->input->is_ajax_request()) {
           exit('No direct script access allowed');
        }

        echo $this->karyawan_model->get_data_karyawan();
    }

}