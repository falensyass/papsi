<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends App_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("master/mahasiswa_model");
    }

    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Mahasiswa');

        if(!$this->ion_auth_acl->has_permission('master-mahasiswa-read')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Mahasiswa', 'Mahasiswa');
        
        $data['list_data'] = $this->mahasiswa_model->get_data()->result();
        $this->layout->view('master/mahasiswa/data_mahasiswa', $data);
    }

    public function add()
    {
        $form = $this->_populate_form($this->input->post());
        $data = array(
            'form'  => $form,
            'title' => 'Tambah Mahasiswa',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Tambah Mahasiswa')
        );

        if(!$this->ion_auth_acl->has_permission('master-mahasiswa-create')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $check = $this->mahasiswa_model->check_mahasiswa_by_id($post['nrp']);
            if($check->num_rows() > 0){
                $this->session->set_flashdata('error', 'Data telah tersedia.');
                redirect('master/mahasiswa', 'refresh');
            } else {
                $this->mahasiswa_model->insert($post);
                $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
                redirect('master/mahasiswa', 'refresh');
            }
        } else {
            $this->layout->view('master/mahasiswa/form_mahasiswa', $data);
        }
    }

    public function edit($id)
    {
        if (!empty($this->input->post())){
            $data = $this->input->post(); 
        } else {
            $data = $this->mahasiswa_model->get_data_by_id($id);
        }

        $form = $this->_populate_form($data);

        $data = array(
            'form'          => $form,
            'title'         => 'Edit Mahasiswa',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Edit Mahasiswa'),
            'url_form'      => base_url('master/mahasiswa/edit/'.$id)
        );

        if(!$this->ion_auth_acl->has_permission('master-mahasiswa-update')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $this->mahasiswa_model->update($id, $post);
            $this->session->set_flashdata('success', 'Data telah berhasil dirubah.');
            redirect('master/mahasiswa', 'refresh');
        } else {
            $this->layout->view('master/mahasiswa/form_mahasiswa', $data);
        }
    }

    public function delete($id)
    { 
        $data = array('title' => 'Hapus Mahasiswa');

        if(!$this->ion_auth_acl->has_permission('master-mahasiswa-delete')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        $this->mahasiswa_model->delete($id);
        $this->session->set_flashdata('success', 'Data telah berhasil dihapus.');
        redirect('master/mahasiswa', 'refresh');
    }

    private function get_all_kelas(){
        $data = $this->mahasiswa_model->get_all_kelas()->result();
        
        foreach ($data as $key){
            $result[$key->id] = $key->kelas;
        }
        return $result;
    }

    public function user()
    {
        $data = array('title' => 'Create User Mahasiswa');

        if(!$this->ion_auth_acl->has_permission('master-mahasiswa-create')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        $mahasiswa = array();
        $mahasiswa_sql = $this->mahasiswa_model->get_data();
        $mahasiswa = $mahasiswa_sql->result_array();
        if($mahasiswa_sql->num_rows() > 0){
            foreach ($mahasiswa as $key){
                $check_user = $this->mahasiswa_model->check_user_mahasiswa_by_id($key['nrp']);
                if($check_user->num_rows() > 0){
                    if(!empty($key['email'])){
                        $email = $key['email'];
                    } else {
                        $email = $key['nrp'].'@example.com';
                    }
                    $username = $key['nrp'];
                    $firstname = $key['nama'];
                    $this->mahasiswa_model->update_user_mahasiswa($username, $email, $firstname);
                } else{
                    if(!empty($key['email'])){
                        $email = $key['email'];
                    } else {
                        $email = $key['nrp'].'@example.com';
                    }
                    $username = $key['nrp'];
                    $password = $key['nrp'];
                    $additional_data = [
                        'first_name' => $key['nama'],
                        'company' => 'SISWA',
                    ];
                    $group = array('6');
                    $this->ion_auth->register($username, $password, $email, $additional_data, $group);
                }
            }
            $this->session->set_flashdata('success', 'Sinkronisasi data telah berhasil.');
        }
        redirect('master/mahasiswa', 'refresh');
    }

    public function _populate_form($data)
    {
        $req_sign = ' <span class="text-danger"> * </span>';
        $form = array(
            'nrp' => array(
                'label' => 'NRP'.$req_sign,
                'input' => array(
                    'name'          => 'nrp',
                    'id'            => 'nrp',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan NRP'
                )
            ),
            'nama' => array(
                'label'  => 'Nama'.$req_sign,
                'input' => array(
                    'name'          => 'nama',
                    'id'            => 'nama',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Nama'
                )
            ),
            'alamat' => array(
                'label'  => 'Alamat'.$req_sign,
                'input' => array(
                    'name'          => 'alamat',
                    'id'            => 'alamat',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Alamat'
                )
            ),
            'hp' => array(
                'label'  => 'No. HP'.$req_sign,
                'input' => array(
                    'name'          => 'hp',
                    'id'            => 'hp',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan HP'
                )
            ),
            'jk' => array(
                'label'  => 'Jenis Kelamin'.$req_sign,
                'input' => array(
                    'name'          => 'jk',
                    'id'            => 'jk',
                    'class'         => 'form-control app-select2',
                    'options'        => array(
                        'Pilih Jenis Kelamin',
                        'Laki-Laki' => 'Laki-Laki',
                        'Perempuan' => 'Perempuan',
                    )
                )
            ),
            'tempat_lahir' => array(
                'label'  => 'Tempat Lahir'.$req_sign,
                'input' => array(
                    'name'          => 'tempat_lahir',
                    'id'            => 'tempat_lahir',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Tempat Lahir'
                )
            ),
            'tgl_lahir' => array(
                'label'  => 'Tanggal Lahir'.$req_sign,
                'input' => array(
                    'name'          => 'tgl_lahir',
                    'id'            => 'tgl_lahir',
                    'class'         => 'form-control app-datepicker',
                    'placeholder'   => 'Masukkan Tanggal Lahir'
                )
            ),
            'email' => array(
                'label'  => 'E-mail'.$req_sign,
                'input' => array(
                    'name'          => 'email',
                    'id'            => 'email',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Email'
                )
            ),
            'asal_sekolah' => array(
                'label'  => 'Asal Sekolah'.$req_sign,
                'input' => array(
                    'name'          => 'asal_sekolah',
                    'id'            => 'asal_sekolah',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Asal Sekolah'
                )
            ),
            'asal_kota' => array(
                'label'  => 'Asal Kota',
                'input' => array(
                    'name'          => 'asal_kota',
                    'id'            => 'asal_kota',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Asal Kota'
                )
            ),
            'nama_ortu' => array(
                'label'  => 'Nama Orang Tua',
                'input' => array(
                    'name'          => 'nama_ortu',
                    'id'            => 'nama_ortu',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Nama Orang Tua'
                )
            ),
            'alamat_ortu' => array(
                'label'  => 'Alamat Orang Tua',
                'input' => array(
                    'name'          => 'alamat_ortu',
                    'id'            => 'alamat_ortu',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Alamat Orang Tua'
                )
            ),
            'hp_ortu' => array(
                'label'  => 'No. HP Orang Tua',
                'input' => array(
                    'name'          => 'hp_ortu',
                    'id'            => 'hp_ortu',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan HP Orang Tua'
                )
            ),
            'kelas' => array(
                'label'     => 'Kelas'.$req_sign,
                'name'      => 'kelas',
                'option'    => $this->get_all_kelas(),
                'selected'  => isset($data['kelas']) ? $data['kelas'] : '',
                'extra'     => array(
                    'id'            => 'kelas',
                    'class'         => 'form-control app-select2',
                )
            ),
            'tahun_masuk' => array(
                'label'  => 'Tahun Masuk',
                'input' => array(
                    'name'          => 'tahun_masuk',
                    'id'            => 'tahun_masuk',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Tahun Masuk'
                )
            ),
            'tahun_keluar' => array(
                'label'  => 'Tahun Keluar',
                'input' => array(
                    'name'          => 'tahun_keluar',
                    'id'            => 'tahun_keluar',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Tahun Keluar'
                )
            ),
        );

        $this->form_validation->set_rules('nrp', 'NRP', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('hp', 'No. HP', 'required');
        $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('email', 'E-mail', 'required');
        $this->form_validation->set_rules('asal_sekolah', 'Asal Sekolah', 'required');
        $this->form_validation->set_rules('kelas', 'Kelas', 'required');
        foreach ($data as $key => $value) {
            $form_select = array ('jk', 'kelas');
            if (in_array($key, $form_select)){
                $form[$key]['input']['selected'] = $value;
            }
            else {
               $form[$key]['input']['value'] = $value;
            }
        }

        return $form;
    }

}