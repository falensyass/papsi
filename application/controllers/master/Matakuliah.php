<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Matakuliah extends App_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("master/matakuliah_model"); 
    }

    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Mata Kuliah');

        if(!$this->ion_auth_acl->has_permission('master-matakuliah-read')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Mata Kuliah', 'Mata Kuliah');
        
        $data['list_data'] = $this->matakuliah_model->get_data()->result();
        $this->layout->view('master/matakuliah/data_matakuliah', $data);
    }

    public function add()
    {
        $form = $this->_populate_form($this->input->post());
        $data = array(
            'form'  => $form,
            'title' => 'Tambah Mata Kuliah',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Tambah Mata Kuliah')
        );

        if(!$this->ion_auth_acl->has_permission('master-mahasiswa-create')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $this->matakuliah_model->insert($post);
            $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
            redirect('master/matakuliah', 'refresh');
        } else {
            $this->layout->view('master/matakuliah/form_matakuliah', $data);
        }
    }

    public function edit($id)
    {
        if (!empty($this->input->post())){
            $data = $this->input->post(); 
        } else {
            $data = $this->matakuliah_model->get_data_by_id($id);
        }

        $form = $this->_populate_form($data);

        $data = array(
            'form'          => $form,
            'title'         => 'Edit Mata Kuliah',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Edit Mata Kuliah'),
            'url_form'      => base_url('master/matakuliah/edit/'.$id)
        );

        if(!$this->ion_auth_acl->has_permission('master-matakuliah-update')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $this->matakuliah_model->update($id, $post);
            $this->session->set_flashdata('success', 'Data telah berhasil dirubah.');
            redirect('master/matakuliah', 'refresh');
        } else {
            $this->layout->view('master/matakuliah/form_matakuliah', $data);
        }
    }

    public function delete($id)
    {
        $data = array('title' => 'Hapus Matakuliah');

        if(!$this->ion_auth_acl->has_permission('master-matakuliah-delete')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        $this->matakuliah_model->delete($id);
        $this->session->set_flashdata('success', 'Data telah berhasil dihapus.');
        redirect('master/matakuliah', 'refresh');
    }

    public function _populate_form($data)
    {
        $req_sign = ' <span class="text-danger"> * </span>';
        $form = array(
            'nama' => array(
                'label'  => 'Nama'.$req_sign,
                'input' => array(
                    'name'          => 'nama',
                    'id'            => 'nama',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Nama Mata Kuliah'
                )
            ),
            'sks' => array(
                'label'  => 'SKS'.$req_sign,
                'input' => array(
                    'name'          => 'sks',
                    'id'            => 'sks',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan SKS'
                )
            ),
            'semester' => array(
                'label'  => 'Semester'.$req_sign,
                'input' => array(
                    'name'          => 'semester',
                    'id'            => 'semester',
                    'class'         => 'form-control app-select2',
                    'options'        => array(
                        'Pilih Semester',
                        'Ganjil' => 'Ganjil',
                        'Genap' => 'Genap',
                    )
                )
            ),
            'tahun_ajaran' => array(
                'label'  => 'Tahun Ajaran'.$req_sign,
                'input' => array(
                    'name'          => 'tahun_ajaran',
                    'id'            => 'tahun_ajaran',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Tahun Ajaran'
                )
            ),
        );

        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('sks', 'SKS', 'required');
        $this->form_validation->set_rules('semester', 'Semester', 'required');
        $this->form_validation->set_rules('tahun_ajaran', 'Tahun Ajaran', 'required');

        foreach ($data as $key => $value) {
            $form_select = array ('semester');
            if (in_array($key, $form_select)){
                $form[$key]['input']['selected'] = $value;
            }
            else {
               $form[$key]['input']['value'] = $value;
            }
        }
        return $form;
    }

}