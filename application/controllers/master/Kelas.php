<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends App_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("master/kelas_model");   
    }

    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Kelas');

        if(!$this->ion_auth_acl->has_permission('master-kelas-read')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Kelas', 'Kelas');
        
        $data['list_data'] = $this->kelas_model->get_data()->result();
        $this->layout->view('master/kelas/data_kelas', $data);
    }

    public function add()
    {
        $form = $this->_populate_form($this->input->post());
        $data = array(
            'form'  => $form,
            'title' => 'Tambah Kelas',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Tambah Kelas')
        );
        
        if(!$this->ion_auth_acl->has_permission('master-kelas-create')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $this->kelas_model->insert($post);
            $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
            redirect('master/kelas', 'refresh');
        } else {
            $this->layout->view('master/kelas/form_kelas', $data);
        }
    }

    public function edit($id)
    {
        if (!empty($this->input->post())){
            $data = $this->input->post(); 
        } else {
            $data = $this->kelas_model->get_data_by_id($id);
        }

        $form = $this->_populate_form($data);

        $data = array(
            'form'          => $form,
            'title'         => 'Edit Kelas',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Edit Kelas'),
            'url_form'      => base_url('master/kelas/edit/'.$id)
        );

        if(!$this->ion_auth_acl->has_permission('master-kelas-update')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $this->kelas_model->update($id, $post);
            $this->session->set_flashdata('success', 'Data telah berhasil dirubah.');
            redirect('master/kelas', 'refresh');
        } else {
            $this->layout->view('master/kelas/form_kelas', $data);
        }
    }

    public function _populate_form($data)
    {
        $req_sign = ' <span class="text-danger"> * </span>';
        $form = array(
            'kelas' => array(
                'label' => 'Kelas'.$req_sign,
                'input' => array(
                    'name'          => 'kelas',
                    'id'            => 'kelas',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Kelas'
                )
            ),
        );

        $this->form_validation->set_rules('kelas', 'Kelas', 'required');

        foreach ($data as $key => $value) {
            $form_select = array ('jk');
            if (in_array($key, $form_select)){
                $form[$key]['input']['selected'] = $value;
            }
            else {
               $form[$key]['input']['value'] = $value;
            }
        }
        
        return $form;
    }

}