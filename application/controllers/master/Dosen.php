<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends App_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("master/dosen_model");   
    }

    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Dosen');

        if(!$this->ion_auth_acl->has_permission('master-dosen-read')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Dosen', 'Dosen');
        
        $data['list_data'] = $this->dosen_model->get_data()->result();
        $this->layout->view('master/dosen/data_dosen', $data);
    }

    public function add()
    {
        $form = $this->_populate_form($this->input->post());
        $data = array(
            'form'  => $form,
            'title' => 'Tambah Dosen',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Tambah Dosen')
        );
        
        if(!$this->ion_auth_acl->has_permission('master-dosen-create')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $check = $this->dosen_model->check_dosen_by_id($post['nip']);
            if($check->num_rows() > 0){
                $this->session->set_flashdata('error', 'Data telah tersedia.');
                redirect('master/dosen', 'refresh');
            } else {
                $this->dosen_model->insert($post);
                $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
                redirect('master/dosen', 'refresh');
            }
        } else {
            $this->layout->view('master/dosen/form_dosen', $data);
        }
    }

    public function edit($id)
    {
        if (!empty($this->input->post())){
            $data = $this->input->post(); 
        } else {
            $data = $this->dosen_model->get_data_by_id($id);
        }

        $form = $this->_populate_form($data);

        $data = array(
            'form'          => $form,
            'title'         => 'Edit Dosen',
            'breadcrumbs'   => $this->layout->get_breadcrumbs('Edit Dosen'),
            'url_form'      => base_url('master/dosen/edit/'.$id)
        );

        if(!$this->ion_auth_acl->has_permission('master-dosen-update')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $this->dosen_model->update($id, $post);
            $this->session->set_flashdata('success', 'Data telah berhasil dirubah.');
            redirect('master/dosen', 'refresh');
        } else {
            $this->layout->view('master/dosen/form_dosen', $data);
        }
    }

    public function delete($id)
    {
        $data = array('title' => 'Hapus Dosen');

        if(!$this->ion_auth_acl->has_permission('master-dosen-delete')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        $this->dosen_model->delete($id);
        $this->session->set_flashdata('success', 'Data telah berhasil dihapus.');
        redirect('master/dosen', 'refresh');
    }

    public function user()
    {
        $data = array('title' => 'Create User Dosen');

        if(!$this->ion_auth_acl->has_permission('master-dosen-create')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        $dosen = array();
        $dosen_sql = $this->dosen_model->get_data();
        $dosen = $dosen_sql->result_array();
        if($dosen_sql->num_rows() > 0){
            foreach ($dosen as $key){
                $check_user = $this->dosen_model->check_user_dosen_by_id($key['nip']);
                if($check_user->num_rows() > 0){
                    if(!empty($key['email'])){
                        $email = $key['email'];
                    } else {
                        $email = $key['nip'].'@example.com';
                    }
                    $username = $key['nip'];
                    $firstname = $key['nama'];
                    $this->dosen_model->update_user_dosen($username, $email, $firstname);
                } else{
                    if(!empty($key['email'])){
                        $email = $key['email'];
                    } else {
                        $email = $key['nip'].'@example.com';
                    }
                    $username = $key['nip'];
                    $password = $key['nip'];
                    $additional_data = [
                        'first_name' => $key['nama'],
                        'company' => 'DOSEN',
                    ];
                    $group = array('5');
                    $this->ion_auth->register($username, $password, $email, $additional_data, $group);
                }
            }
            $this->session->set_flashdata('success', 'Sinkronisasi data telah berhasil.');
        }
        redirect('master/dosen', 'refresh');
    }

    public function _populate_form($data)
    {
        $req_sign = ' <span class="text-danger"> * </span>';
        $form = array(
            'nip' => array(
                'label' => 'NIP',
                'input' => array(
                    'name'          => 'nip',
                    'id'            => 'nip',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan NIP'
                )
            ),
            'nama' => array(
                'label'  => 'Nama'.$req_sign,
                'input' => array(
                    'name'          => 'nama',
                    'id'            => 'nama',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Nama'
                )
            ),
            'alamat' => array(
                'label'  => 'Alamat',
                'input' => array(
                    'name'          => 'alamat',
                    'id'            => 'alamat',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Alamat'
                )
            ),
            'hp' => array(
                'label'  => 'No. HP'.$req_sign,
                'input' => array(
                    'name'          => 'hp',
                    'id'            => 'hp',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan HP'
                )
            ),
            'jk' => array(
                'label'  => 'Jenis Kelamin',
                'input' => array(
                    'name'          => 'jk',
                    'id'            => 'jk',
                    'class'         => 'form-control app-select2',
                    'options'        => array(
                        'Pilih Jenis Kelamin',
                        'Laki-Laki' => 'Laki-Laki',
                        'Perempuan' => 'Perempuan',
                    )
                )
            ),
            'tempat_lahir' => array(
                'label'  => 'Tempat Lahir',
                'input' => array(
                    'name'          => 'tempat_lahir',
                    'id'            => 'tempat_lahir',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Tempat Lahir'
                )
            ),
            'tgl_lahir' => array(
                'label'  => 'Tanggal Lahir',
                'input' => array(
                    'name'          => 'tgl_lahir',
                    'id'            => 'tgl_lahir',
                    'class'         => 'form-control app-datepicker',
                    'type'          => 'date',
                    'placeholder'   => 'Masukkan Tanggal Lahir'
                )
            ),
            'email' => array(
                'label'  => 'E-mail'.$req_sign,
                'input' => array(
                    'name'          => 'email',
                    'id'            => 'email',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Email'
                )
            ),
        );

        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('hp', 'HP', 'required');
        $this->form_validation->set_rules('email', 'E-mail', 'required');

        foreach ($data as $key => $value) {
            $form_select = array ('jk');
            if (in_array($key, $form_select)){
                $form[$key]['input']['selected'] = $value;
            }
            else {
               $form[$key]['input']['value'] = $value;
            }
        }
        
        return $form;
    }




}