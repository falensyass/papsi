<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends App_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('dashboard_model');
    }

    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Selamat Datang');

		$data['breadcrumbs'] = array(
            array('title' => 'Beranda', 'url' => base_url()),
			array('title' => 'Selamat Datang di Halaman Beranda', 'url' => base_url()),
        );
        
        if ($this->ion_auth->in_group('6')) {
            $data['card_title'] = 'Jadwal';
            $nrp = $this->ion_auth->user()->row()->username;
            $data['list_jadwal'] = $this->dashboard_model->get_data_mahasiswa($nrp)->result();
        } else if ($this->ion_auth->in_group('5')) {
            $data['card_title'] = 'Jadwal';
            $nip = $this->ion_auth->user()->row()->username;
            $data['list_jadwal'] = $this->dashboard_model->get_data_dosen($nip)->result();
        }
        
        $this->layout->view('dashboard/home', $data);
    }

}