<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| Navigation Menu untuk Halaman Dashboard
| -------------------------------------------------------------------
| Config untuk halaman admin dashboard dengan value array sebagai berikut:
| - icon : icon pada menu (https://coreui.io/demo/#icons/simple-line-icons.html)
| - url : url untuk menu navigasi
| - submenu : untuk menempatkan sub menu dalam format array, kosongi jika menu tidak memiliki submenu
| - permission : nama permission untuk hak akses menu
*/
$config['dashboard'] = array(
    'Beranda' => array(
        'icon'=> 'nav-icon icon-home',
        'url' => base_url('dashboard'),
        'permission' => array(
            'dashboard-beranda-read'
        )
    ),


    'Master' => array(
        'icon' => 'nav-icon icon-grid',
        'submenu'=> array(
            'Dosen'  => array(
                'url' => base_url('master/dosen'),
                'icon'=> 'nav-icon icon-people',
                'permission' => 'master-dosen-read'
            ),

            'Mahasiswa'  => array(
                'url' => base_url('master/mahasiswa'),
                'icon'=> 'nav-icon icon-people',
                'permission' => 'master-mahasiswa-read'
            ),

            'Mata Kuliah'  => array(
                'url' => base_url('master/matakuliah'),
                'icon'=> 'nav-icon icon-book-open',
                'permission' => 'master-matakuliah-read'
            ),

            'Kelas'  => array(
                'url' => base_url('master/kelas'),
                'icon'=> 'nav-icon icon-badge',
                'permission' => 'master-kelas-read'
            ),
        ),
        'permission' => array(
            'master-dosen-read',
            'master-mahasiswa-read',
            'master-matakuliah-read',
            'master-kelas-read'
        ),
    ),

    'Transaksi' => array(
        'icon' => 'nav-icon icon-grid',
        'submenu'=> array(
            'Jadwal Kuliah'  => array(
                'url' => base_url('transaksi/jadwal'),
                'icon'=> 'nav-icon icon-calendar',
                'permission' => 'transaksi-jadwal-read'
            ),

            'Nilai'  => array(
                'url' => base_url('transaksi/nilai'),
                'icon'=> 'nav-icon icon-chart',
                'permission' => 'transaksi-nilai-read'
            ),

            'Kerja Praktik'  => array(
                'url' => base_url('transaksi/kerjapraktik'),
                'icon'=> 'nav-icon icon-notebook',
                'permission' => 'transaksi-kerja_praktik-read'
            ),
        ),
        'permission' => array(
            'transaksi-jadwal-read',
            'transaksi-nilai-read',
            'transaksi-kerja_praktik-read'
        ),
    ),

    'Laporan' => array(
        'icon' => 'nav-icon icon-grid',
        'submenu'=> array(
            'Transkrip Nilai'  => array(
                'url' => base_url('laporan/transkrip'),
                'icon'=> 'nav-icon icon-docs',
                'permission' => 'laporan-transkrip-read'
            ),
        ),
        'permission' => array(
            'laporan-transkrip-read',
            'laporan-sertifikat-read',
        ),
    ),

    'Pengaturan' => array(
        'icon' => 'nav-icon icon-settings',
        'submenu'=> array(
            'Pengguna'  => array(
                'url' => base_url('auth'),
                'icon'=> 'nav-icon icon-user',
                'permission' => 'pengaturan-pengguna-read'
            ),

            'Hak Akses' => array(
                'url' => base_url('permission'),
                'icon'=> 'nav-icon icon-lock',
                'permission' => 'pengaturan-hak_akses-read'
            ),
        ),
        'permission' => array(
            'pengaturan-pengguna-read',
            'pengaturan-hak_akses-read',
        ),
    ),
);


/*
| -------------------------------------------------------------------
| Navigation Menu untuk Halaman Frontend
| -------------------------------------------------------------------
| Config untuk halaman admin dashboard dengan value array sebagai berikut:
| - icon : icon pada menu menggunakan font awesome (https://fontawesome.com/v4.7.0/icons)
| - url : url untuk menu navigasi
| - submenu : untuk menempatkan sub menu dalam format array, kosongi jika menu tidak memiliki submenu
*/
$config['frontend'] = array();

