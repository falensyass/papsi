<div class="row">
  <div class="col-md-12">

  <?php echo show_alert($this->session->flashdata()); ?>
  </div>
</div>

<?php
$form_label = array('class' => 'control-label col-md-2');
?>
<div class="row">
  <div class="col-md-12">
    <?php echo show_alert($this->session->flashdata()); ?>

    <div class="card">
      <?php echo form_open('', array('class' => 'form-horizontal')); ?>
      <div class="card-header">
        <i class="nav-icon icon-flag"></i> <?php echo $title ?>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <?php echo form_label($form['nama']['label'],'nama', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['nama']['input']); ?>
            <?php echo form_error('nama', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['sks']['label'],'sks', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['sks']['input']); ?>
            <?php echo form_error('sks', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['semester']['label'],'semester', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_dropdown($form['semester']['input']); ?>
            <?php echo form_error('semester', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['tahun_ajaran']['label'],'tahun_ajaran', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['tahun_ajaran']['input']); ?>
            <?php echo form_error('tahun_ajaran', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <div class="col-md-10">
            <label class="text-danger">* : Wajib diisi.</label>
          </div>
        </div>
      </div>

      <div class="card-footer">
          <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit'));?>
          <?php echo anchor(base_url('master/matakuliah'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning'));?>
        
      </div>

        <?php echo form_close();?>
    </div>
  </div>
</div>
