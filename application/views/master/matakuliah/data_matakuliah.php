<div class="row">
  <div class="col-md-12">

  <?php echo show_alert($this->session->flashdata()); ?>
  </div>
</div>

<div class="row">
  <div class="col-md-12">

    <?php if (!empty($message_success)) : ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $message_success; ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <?php endif; ?>

    <div class="card">
      <div class="card-header">
        <i class="nav-icon icon-book-open"></i> Data <?php echo show($title); ?>
        <div class="pull-right">
        <?php if($this->ion_auth_acl->has_permission('master-matakuliah-create')): ?>
          <a href="<?php echo base_url('master/matakuliah/add'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah</a>
        <?php endif; ?> 
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-condensed table-datatable">
                <thead>
                  <tr>
                      <th style="width: 10px;">No</th>
                      <th>Nama</th>
                      <th>SKS</th>
                      <th>Semester</th>
                      <th>Tahun Ajaran</th>
                      <?php if($this->ion_auth_acl->has_permission('master-matakuliah-update') || $this->ion_auth_acl->has_permission('master-matakuliah-delete')): ?>
                      <th style="width: 10px;">Pilihan</th>
                      <?php endif; ?>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach($list_data as $data): ?>
                  <tr>
                    <td class="text-center"><?php show($no++); ?></td>
                    <td><?php show($data->nama); ?></td>
                    <td><?php show($data->sks); ?></td>
                    <td><?php show($data->semester); ?></td>
                    <td><?php show($data->tahun_ajaran); ?></td>
                    <?php if($this->ion_auth_acl->has_permission('master-matakuliah-update') || $this->ion_auth_acl->has_permission('master-matakuliah-delete')): ?>
                    <td class="text-center">
                        <div class="dropdown dropleft">
                          <button class="btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-menu"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <?php if($this->ion_auth_acl->has_permission('master-matakuliah-update')): ?>
                            <a class="dropdown-item" href="<?php echo base_url('master/matakuliah/edit/'.$data->id); ?>"><i class="fa fa-edit"></i> Edit</a>
                          <?php endif; ?>
                          <?php if($this->ion_auth_acl->has_permission('master-matakuliah-delete')): ?>
                            <a class="dropdown-item btn-delete" href="<?php echo base_url('master/matakuliah/delete/'.$data->id); ?>"><i class="fa fa-trash"></i> Hapus</a>
                          <?php endif; ?>
                          </div>
                        </div>
                    </td>
                    <?php endif; ?>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
