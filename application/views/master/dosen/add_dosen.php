<div class="card bg-light" style="width: 100%;">
  <div class="card-header">
    Tambah Dosen
  </div>
  <div class="card-body">
    <?php if ($this->session->flashdata('success')): ?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php endif; ?>

    <form method="post" action="" enctype="multipart/form-data">
      <div class="form-group row">
        <label for="nip" class="col-sm-2 col-form-label">NIP</label>
        <div class="col-sm-10">
          <input type="text" class="form-control <?php echo form_error('nip') ? 'is-invalid':'' ?>"  name="nip" placeholder="Masukkan NIP">
          <div class="invalid-feedback">
            <?php echo form_error('nip') ?>
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label for="nama" class="col-sm-2 col-form-label">Nama</label>
        <div class="col-sm-10">
          <input type="text" class="form-control <?php echo form_error('nama') ? 'is-invalid':'' ?>" name="nama" placeholder="Masukkan Nama">
          <div class="invalid-feedback">
            <?php echo form_error('nama') ?>
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label for="jenis_kelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
        <div class="col-sm-10">
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="jenis_kelamin" value="L" checked="checked">
            <label class="form-check-label" for="jenis_kelamin_L">Laki-Laki</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="jenis_kelamin" value="P">
            <label class="form-check-label" for="jenis_kelamin_P">Perempuan</label>
          </div>
          <div class="invalid-feedback">
            <?php echo form_error('jenis_kelamin') ?>
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label for="asal" class="col-sm-2 col-form-label">Asal</label>
        <div class="col-sm-10">
          <input type="text" class="form-control <?php echo form_error('asal') ? 'is-invalid':'' ?>" placeholder="Asal Kota" name="asal">
          <div class="invalid-feedback">
            <?php echo form_error('asal') ?>
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label for="hp" class="col-sm-2 col-form-label">Nomor HP</label>
        <div class="col-sm-10">
          <input type="text" class="form-control <?php echo form_error('hp') ? 'is-invalid':'' ?>" placeholder="Masukkan HP" name="hp">
          <div class="invalid-feedback">
            <?php echo form_error('hp') ?>
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label for="aktif" class="col-sm-2 col-form-label">Aktif</label>
        <div class="col-sm-10">
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="aktif" value="1" checked="checked">
            <label class="form-check-label" for="aktif_1">Ya</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="aktif" value="0">
            <label class="form-check-label" for="aktif_0">Tidak</label>
          </div>
          <div class="invalid-feedback">
            <?php echo form_error('aktif') ?>
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label for="foto" class="col-sm-2 col-form-label">Foto</label>
        <div class="col-sm-10 custom-file">
            <input type="file" class="custom-file-input" name="foto">
            <label class="custom-file-label" for="customFile">Choose file</label>
        </div>
      </div>
      <div class="form-group row">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
      </div>
    </form>
  </div>
  <div class="card-footer small text-muted">
    * required fields
  </div>
</div>
