<div class="row">
  <div class="col-md-12">

  <?php echo show_alert($this->session->flashdata()); ?>
  </div>
</div>

<div class="row">
  <div class="col-md-12">

    <?php if (!empty($message_success)) : ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $message_success; ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <?php endif; ?>

<!-- data tabel -->
      <div class="card">
      <div class="card-header">
        <i class="nav-icon icon-calendar"></i><?php echo show($title); ?>
        <div class="pull-right">
        <?php if(!$this->ion_auth->in_group('5')): ?>
          <a href="<?php echo base_url('transaksi/nilai'); ?>" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
        <?php endif; ?>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-condensed table-datatable">
                <thead>
                  <tr>
                      <th style="width: 10px;">No</th>
                      <th>Hari</th>
                      <th>Kelas</th>
                      <th>Jam Masuk</th>
                      <th>Jam Pulang</th>
                      <th>Mata Kuliah</th>
                      <th>Dosen Pengajar</th>
                      <th style="width: 10px; align: 'center'">Pilihan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach($list_data as $data): ?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <!-- <td><?php echo $row->id_nilai; ?></td> -->
                    <td><?php show($data->hari); ?></td>
                    <td><?php show($data->kelas); ?></td>
                    <td><?php show($data->jam_masuk); ?></td>
                    <td><?php show($data->jam_pulang); ?></td>
                    <td><?php show($data->matkul); ?></td>
                    <td><?php show($data->nama_dsn); ?></td>
                    <td class="text-center">

                        <div class="dropdown dropleft">
                          <button class="btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-menu"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="<?php echo base_url('transaksi/nilai/detail/'.$data->id); ?>"><i class="fa fa-eye"></i> Detail</a>
                          </div>
                        </div>
                        </div>
                    </td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- data table -->
  </div>
</div>
