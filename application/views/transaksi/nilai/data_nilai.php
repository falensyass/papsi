<div class="row">
  <div class="col-md-12">

  <?php echo show_alert($this->session->flashdata()); ?>
  </div>
</div>

<div class="row">
  <div class="col-md-12">

    <?php if (!empty($message_success)) : ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $message_success; ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <?php endif; ?>

<!-- data tabel -->
      <div class="card">
      <div class="card-header">
        <i class="nav-icon icon-chart"></i><?php echo show($title); ?>
        <div class="pull-right">
          <a href="<?php echo base_url('transaksi/nilai/add/'.$id_jadwal); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah</a>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-condensed table-datatable">
                <thead>
                  <tr>
                      <th style="width: 10px;">No</th>
                      <th>NRP</th>
                      <th>Nama</th>
                      <th>Nilai Tugas</th>
                      <th>Nilai UTS</th>
                      <th>Nilai UAS</th>
                      <th>Nilai Huruf</th>
                      <th style="width: 10px;">Pilihan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach($list_data as $row): ?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <!-- <td><?php echo $row->id_nilai; ?></td> -->
                    <td><?php echo $row['nrp']; ?></td>
                    <td><?php echo $row['nama']; ?></td>
                    <td><?php echo $row['tugas']; ?></td>
                    <td><?php echo $row['uts']; ?></td>
                    <td><?php echo $row['uas']; ?></td>
                    <td><?php echo $row['huruf']; ?></td>
                    <?php if(!empty($row['id_nilai'])) : ?>
                        <td class="text-center">
                          <div class="dropdown dropleft">
                            <button class="btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="icon-menu"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                              <a class="dropdown-item" href="<?php echo base_url('transaksi/nilai/edit/'.$row['id_jadwal'].'/'.$row['id_nilai']); ?>"><i class="fa fa-edit"></i> Edit</a>
                              <a class="dropdown-item btn-delete" href="<?php echo base_url('transaksi/nilai/delete/'.$row['id_jadwal'].'/'.$row['id_nilai']); ?>"><i class="fa fa-trash"></i> Hapus</a>
                            </div>
                          </div>
                      </td>
                    <?php else : ?>
                      <td class="text-center">-</td>
                    <?php endif; ?>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- data table -->
  </div>
</div>
