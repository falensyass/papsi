<div class="row">
  <div class="col-md-12">

  <?php echo show_alert($this->session->flashdata()); ?>
  </div>
</div>

<?php
$form_label = array('class' => 'control-label col-md-2');
?>
<div class="row">
  <div class="col-md-12">
    <?php echo show_alert($this->session->flashdata()); ?>

    <div class="card">
      <?php echo form_open('', array('class' => 'form-horizontal')); ?>
      <div class="card-header">
        <i class="nav-icon icon-flag"></i> <?php echo $title ?>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <?php echo form_label($form['id_mhs']['label'],'id_mhs', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_dropdown($form['id_mhs']['name'], $form['id_mhs']['option'], $form['id_mhs']['selected'],$form['id_mhs']['extra']); ?>
            <?php echo form_error('id_mhs', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['nilai_tugas']['label'],'nilai_tugas', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['nilai_tugas']['input']); ?>
            <?php echo form_error('nilai_tugas', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['nilai_uts']['label'],'nilai_uts', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['nilai_uts']['input']); ?>
            <?php echo form_error('nilai_uts', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['nilai_uas']['label'],'nilai_uas', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['nilai_uas']['input']); ?>
            <?php echo form_error('nilai_uas', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <div class="col-md-10">
            <label class="text-danger">* : Wajib diisi.</label>
          </div>
        </div>
      </div>

      <div class="card-footer">
          <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit'));?>
          <?php echo anchor(base_url('transaksi/nilai'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning'));?>
        
      </div>

        <?php echo form_close();?>
    </div>
  </div>
</div>
<script type="text/javascript">
            $(document).ready(function(){

                // Format mata uang.
                $( '.time' ).mask("00:00:00", {reverse: true});

            })
        </script>