<div class="row">
  <div class="col-md-12">

  <?php echo show_alert($this->session->flashdata()); ?>
  </div>
</div>

<div class="row">
  <div class="col-md-12">

    <?php if (!empty($message_success)) : ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $message_success; ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <?php endif; ?>

<!-- data tabel -->
      <div class="card">
        <form class="form-horizontal" action="<?php echo base_url('transaksi/nilai/insert/'.$id_jadwal)?>" method="post">
      <div class="card-header">
        <i class="nav-icon icon-chart"></i> Data <?php echo show($title); ?>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-condensed table-datatable">
                <thead>
                  <tr>
                      <th style="width: 10px;">No</th>
                      <th>NRP</th>
                      <th>Nama</th>
                      <th>Nilai Tugas</th>
                      <th>Nilai UTS</th>
                      <th>Nilai UAS</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach($list_data as $row): ?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <!-- <td><?php echo $row->id_nilai; ?></td> -->
                    <td><?php echo $row['nrp']; ?></td>
                    <td><?php echo $row['nama']; ?></td>
                    <input type="hidden" name="id_jadwal[]" value="<?php show($id_jadwal) ?>">
                    <input type="hidden" name="id_mhs[]" value="<?php show($row['id_mhs']) ?>">
                    <td><input class="form-control" type="text" name="nilai_tugas[]" value="<?php show($row['tugas']) ?>"></td>
                    <td><input class="form-control" type="text" name="nilai_uts[]" value="<?php show($row['uts']) ?>"></td>
                    <td><input class="form-control" type="text" name="nilai_uas[]" value="<?php show($row['uas']) ?>"></td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="card-footer">
          <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Submit', 'class' => 'btn btn-primary', 'type' => 'submit'));?>
          <?php echo anchor(base_url('transaksi/nilai/detail/'.$id_jadwal), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning'));?>
      </div>
        </form>
    </div>
    <!-- data table -->
  </div>
</div>
