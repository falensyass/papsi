<div class="row">
  <div class="col-md-12">

  <?php echo show_alert($this->session->flashdata()); ?>
  </div>
</div>

<?php
$form_label = array('class' => 'control-label col-md-2');
?>
<div class="row">
  <div class="col-md-12">
    <?php echo show_alert($this->session->flashdata()); ?>

    <div class="card">
      <?php echo form_open('', array('class' => 'form-horizontal')); ?>
      <div class="card-header">
        <i class="nav-icon icon-flag"></i> <?php echo $title ?>
      </div>
      <div class="card-body">
        
        <div class="form-group row">
          <?php echo form_label($form['id_matkul']['label'],'id_matkul', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_dropdown($form['id_matkul']['name'], $form['id_matkul']['option'], $form['id_matkul']['selected'],$form['id_matkul']['extra']); ?>
            <?php echo form_error('id_matkul', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['id_dosen']['label'],'id_dosen', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_dropdown($form['id_dosen']['name'], $form['id_dosen']['option'], $form['id_dosen']['selected'],$form['id_dosen']['extra']); ?>
            <?php echo form_error('id_dosen', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['kelas']['label'],'kelas', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_dropdown($form['kelas']['name'], $form['kelas']['option'], $form['kelas']['selected'],$form['kelas']['extra']); ?>
            <?php echo form_error('kelas', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['hari']['label'],'hari', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_dropdown($form['hari']['input']); ?>
            <?php echo form_error('hari', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['jam_masuk']['label'],'jam_masuk', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['jam_masuk']['input']); ?>
            <?php echo form_error('jam_masuk', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['jam_pulang']['label'],'jam_pulang', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['jam_pulang']['input']); ?>
            <?php echo form_error('jam_pulang', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <div class="col-md-10">
            <label class="text-danger">* : Wajib diisi.</label>
          </div>
        </div>
      </div>

      <div class="card-footer">
          <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit'));?>
          <?php echo anchor(base_url('transaksi/jadwal'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning'));?>
        
      </div>

        <?php echo form_close();?>
    </div>
  </div>
</div>
<script type="text/javascript">
            $(document).ready(function(){

                // Format mata uang.
                $( '.time' ).mask("00:00:00", {reverse: true});

            })
        </script>