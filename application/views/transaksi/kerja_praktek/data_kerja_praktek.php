<div class="row">
  <div class="col-md-12">

    <?php if (!empty($message_success)) : ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $message_success; ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <?php endif; ?>

    <div class="card">
      <div class="card-header">
        <i class="nav-icon icon-people"></i> Data <?php echo show($title); ?>
        <div class="pull-right">
          <a href="<?php echo base_url('transaksi/kerja_praktek/tambah-kerja_praktek'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah</a>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-condensed table-datatable">
                <thead>
                  <tr>
                      <th style="width: 10px;">No</th>
                      <th>Judul</th>
                      <th>Perusahaan</th>
                      <th>Tanggal Mulai</th>
                      <th>Tanggal Selesai</th>
                      <!-- <th>Kelas</th> -->
                      <th style="width: 10px;">Pilihan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach($kerja_praktek as $row): ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $row->judul; ?></td>
                    <td><?php echo $row->perusahaan; ?></td>
                    <td><?php echo $row->tgl_mulai; ?></td>
                    <td><?php echo $row->tgl_selesai; ?></td>
                    <!-- <td><?php echo $row->kelas; ?></td> -->
                    <td class="text-center">

                        <div class="dropdown dropleft">
                          <button class="btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-menu"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="<?php echo base_url('transaksi/update-kerja_praktek/'.$row->id_kerja_praktek); ?>"><i class="fa fa-edit"></i> Edit</a>
                            <a class="dropdown-item btn-delete" href="<?php echo base_url('transaksi/delete-kerja_praktek/'.$row->id_kerja_praktek); ?>"><i class="fa fa-trash"></i> Hapus</a>
                          </div>
                        </div>
                    </td>
                  </tr>
                  <?php $no++; endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
