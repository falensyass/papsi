<div class="row">
  <div class="col-md-12">

  <?php echo show_alert($this->session->flashdata()); ?>
  </div>
</div>

<?php
$form_label = array('class' => 'control-label col-md-2');
?>
<div class="row">
  <div class="col-md-12">
    <?php echo show_alert($this->session->flashdata()); ?>

    <div class="card">
      <?php echo form_open('', array('class' => 'form-horizontal')); ?>
      <div class="card-header">
        <i class="nav-icon icon-flag"></i> <?php echo $title ?>
      </div>
      <div class="card-body">
        
        <div class="form-group row">
          <?php echo form_label($form['judul_TA']['label'],'judul_TA', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['judul_TA']['input']); ?>
            <?php echo form_error('judul_TA', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['perusahaan']['label'],'perusahaan', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['perusahaan']['input']); ?>
            <?php echo form_error('perusahaan', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['id_dosen_pembimbing']['label'],'id_dosen_pembimbing', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_dropdown($form['id_dosen_pembimbing']['name'], $form['id_dosen_pembimbing']['option'], $form['id_dosen_pembimbing']['selected'],$form['id_dosen_pembimbing']['extra']); ?>
            <?php echo form_error('id_dosen_pembimbing', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['id_dosen_penguji']['label'],'id_dosen_penguji', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_dropdown($form['id_dosen_penguji']['name'], $form['id_dosen_penguji']['option'], $form['id_dosen_penguji']['selected'],$form['id_dosen_penguji']['extra']); ?>
            <?php echo form_error('id_dosen_penguji', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <div class="col-md-10">
            <label class="text-danger">* : Wajib diisi.</label>
          </div>
        </div>
      </div>

      <div class="card-footer">
          <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit'));?>
          <?php echo anchor(base_url('transaksi/kerjapraktik'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning'));?>
        
      </div>

        <?php echo form_close();?>
    </div>
  </div>
</div>
<script type="text/javascript">
            $(document).ready(function(){

                // Format mata uang.
                $( '.time' ).mask("00:00:00", {reverse: true});

            })
        </script>