<div class="row">
  <div class="col-md-12">

  <?php echo show_alert($this->session->flashdata()); ?>
  </div>
</div>

<div class="row">
  <div class="col-md-12">

    <?php if (!empty($message_success)) : ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $message_success; ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <?php endif; ?>

    <div class="card">
      <div class="card-header">
        <i class="nav-icon icon-notebook"></i> Data <?php echo show($title); ?>
        <div class="pull-right">
        <?php if($this->ion_auth_acl->has_permission('transaksi-kerja_praktik-create')): ?>
          <a href="<?php echo base_url('transaksi/kerjapraktik/add'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah</a>
        <?php endif; ?>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-condensed table-datatable">
                <thead>
    		          <tr>
                    <th style="width: 10px;">No</th>
                    <th>Judul</th>
                    <th>Perusahaan</th>
                    <th>Dosen Pembimbing</th>
                    <th>Dosen Penguji</th>
                    <th>Mahasiswa</th>
                    <?php if($this->ion_auth_acl->has_permission('transaksi-kerja_praktik-update') || $this->ion_auth_acl->has_permission('transaksi-kerja_praktik-delete') || $this->ion_auth_acl->has_permission('transaksi-kerja_praktik-anggota-create') || $this->ion_auth_acl->has_permission('transaksi-kerja_praktik-nilai-create')): ?>
                    <th style="width: 10px;">Pilihan</th>
                    <?php endif; ?>
                  </tr>
    	          </thead>
                <tbody>
                  <?php $no=1; foreach($list_data as $data): ?>
                  <tr>
                    <td class="text-center"><?php show($no++); ?></td>
                    <td><?php show($data->judul_TA); ?></td>
                    <td><?php show($data->perusahaan); ?></td>
                    <td><?php show($data->nama_pembimbing); ?></td>
                    <td><?php show($data->nama_penguji); ?></td>
                    <td>
                      <div class="row">
                       
                          
                      <?php foreach($list_mhs as $val): ?>
                        <?php if ($data->id==$val->id_kp): ?>
                          
                         <div class="col-sm-3">
                           <p><?php show($val->nrp) ?></p>
                         </div>
                         <div class="col-sm-1">|</div>
                         <div class="col-sm-4">
                           <p><?php show($val->nama) ?></p>
                           
                         </div>
                         <div class="col-sm-4">
                           <p><?php show($val->nilai) ?></p>
                           
                         </div>
                          
                        <?php endif; ?>
                      <?php endforeach; ?>
                      
                       
                      </div>
                    </td>
                    <?php if($this->ion_auth_acl->has_permission('transaksi-kerja_praktik-update') || $this->ion_auth_acl->has_permission('transaksi-kerja_praktik-delete') || $this->ion_auth_acl->has_permission('transaksi-kerja_praktik-anggota-create') || $this->ion_auth_acl->has_permission('transaksi-kerja_praktik-nilai-create')): ?>
                    <td class="text-center">
                        <div class="dropdown dropleft">
                          <button class="btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-menu"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <?php if($this->ion_auth_acl->has_permission('transaksi-kerja_praktik-anggota-create')): ?>
                            <a class="dropdown-item" href="<?php echo base_url('transaksi/kerjapraktik/anggota/'.$data->id); ?>"><i class="fa fa-user"></i> Anggota</a>
                          <?php endif; ?>
                          <?php if($this->ion_auth_acl->has_permission('transaksi-kerja_praktik-nilai-create')): ?>
                            <a class="dropdown-item" href="<?php echo base_url('transaksi/kerjapraktik/nilai/'.$data->id); ?>"><i class="fa fa-book"></i> Nilai</a>
                          <?php endif; ?>
                          <?php if($this->ion_auth_acl->has_permission('transaksi-kerja_praktik-update')): ?>  
                            <a class="dropdown-item" href="<?php echo base_url('transaksi/kerjapraktik/edit/'.$data->id); ?>"><i class="fa fa-edit"></i> Edit</a>
                          <?php endif; ?>
                          <?php if($this->ion_auth_acl->has_permission('transaksi-kerja_praktik-delete')): ?>
                            <a class="dropdown-item btn-delete" href="<?php echo base_url('transaksi/kerjapraktik/delete/'.$data->id); ?>"><i class="fa fa-trash"></i> Hapus</a>
                          <?php endif; ?>
                          </div>
                        </div>
                        </div>
                    </td>
                    <?php endif; ?>
                  </tr>
                  <?php endforeach; ?>
              </tbody>
	          </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>