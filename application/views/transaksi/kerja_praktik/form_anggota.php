<div class="row">
  <div class="col-md-12">

  <?php echo show_alert($this->session->flashdata()); ?>
  </div>
</div>

<?php
    $form_label = array('class' => 'control-label col-md-2');
?>
<div class="row">
  <div class="col-md-12">
    <?php echo show_alert($this->session->flashdata()); ?>
    <div class="card">
      <?php echo form_open('', array('class' => 'form-horizontal')); ?>
      <div class="card-header">
        <i class="nav-icon icon-flag"></i> <?php echo $title ?>
      </div>
      <div class="card-body">
        <!-- Awal data anggota -->
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-condensed table-datatable">
                <thead>
    		      <tr>
                    <th class="align-middle" style="width: 10px;"></th>
                    <th class="align-middle" style="width: 10px;">No</th>
                    <th>NRP</th>
                    <th>Nama</th>
                    <th>No HP</th>
                    <th>Email</th>
                  </tr>
    	        </thead>
                <tbody>
                  <?php $no=1; foreach($mahasiswa as $data): ?>
                  <tr>          
                    <td><input type="checkbox" name="id_mhs[]" value="<?php show($data->id) ?>" /></td>
                    <td class="text-center"><?php show($no++); ?></td>
                    <td><?php show($data->nrp); ?></td>
                    <td><?php show($data->nama); ?></td>
                    <td><?php show($data->hp); ?></td>
                    <td><?php show($data->email); ?></td>
                  </tr>
                  <?php endforeach; ?>
              </tbody>
	          </table>
           </div>
          </div>
        </div>
        <!-- /Akhir data anggota -->

        <div class="card-footer">
            <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit'));?>
            <?php echo anchor(base_url('transaksi/kerjapraktik'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning'));?>
        </div>
    <?php echo form_close();?>
    </div>
  </div>
</div>