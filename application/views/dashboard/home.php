<div class="row">
  <div class="col-md-12">

  <?php echo show_alert($this->session->flashdata()); ?>

  </div>
</div>

<div class="row">
    <div class="col-md-12">
    <?php if($this->ion_auth->in_group('5')) : ?>
        <div class="jumbotron bg-light">
            <div class="container">
                <h1 class="display-5 text-success">Selamat Datang, <?php echo $this->ion_auth->user()->row()->first_name; ?></h1>
                <p class="lead text-success">SIM PAPSI-ITS - <?php echo  $this->ion_auth->get_users_groups()->row()->description ?></p>
            </div>
        </div>
    <?php elseif($this->ion_auth->in_group('6')) : ?>
        <div class="jumbotron bg-light">
            <div class="container">
                <h1 class="display-5 text-primary">Selamat Datang, <?php echo $this->ion_auth->user()->row()->first_name; ?></h1>
                <p class="lead text-primary">SIM PAPSI-ITS - <?php echo  $this->ion_auth->get_users_groups()->row()->description ?></p>
            </div>
        </div>
    <?php else : ?>
        <div class="jumbotron bg-light">
            <div class="container">
                <h1 class="display-5 text-danger">Selamat Datang, <?php echo $this->ion_auth->user()->row()->first_name; ?></h1>
                <p class="lead text-danger">SIM PAPSI-ITS - <?php echo  $this->ion_auth->get_users_groups()->row()->description ?></p>
            </div>
        </div>
    <?php endif; ?>
    </div>
</div>

<div class="row">
    <!-- awal data jadwal -->
    <div class="col-md-6">
    <?php if($this->ion_auth->in_group('6') || $this->ion_auth->in_group('5')) : ?>
        <div class="card">
            <div class="card-header">
                <i class="nav-icon icon-calendar"></i> <?php echo show($card_title); ?>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-condensed table-datatable">
                                <thead>
                                <tr>
                                    <th style="width: 10px;">No</th>
                                    <th>Mata Kuliah</th>
                                    <th>Hari</th>
                                    <?php if($this->ion_auth->in_group('6')) : ?>
                                    <th>Dosen</th>
                                    <?php elseif($this->ion_auth->in_group('5')) : ?>
                                    <th>Kelas</th>
                                    <?php endif; ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $no=1; foreach($list_jadwal as $data): ?>
                                <tr>
                                    <td class="text-center"><?php show($no++); ?></td>
                                    <td><?php show($data->nama_mk); ?></td>
                                    <td><?php show($data->hari); ?></td>
                                    <?php if($this->ion_auth->in_group('6')) : ?>
                                    <td><?php show($data->nama_dsn); ?></td> 
                                    <?php elseif($this->ion_auth->in_group('5')) : ?>
                                    <td><?php show($data->kelas); ?></td> 
                                    <?php endif; ?>
                                </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    </div>
    <!-- akhir data jadwal -->
</div>