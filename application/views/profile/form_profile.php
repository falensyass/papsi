<?php
$form_label = array('class' => 'control-label col-md-2');
?>
<div class="row">
  <div class="col-md-12">
    <?php echo show_alert($this->session->flashdata()); ?>

    <div class="card">
      <?php echo form_open('', array('class' => 'form-horizontal')); ?>
      <div class="card-header">
        <i class="nav-icon icon-people"></i> <?php echo $title ?>
      </div>
      <div class="card-body">
        
        <div class="form-group row">
          <?php echo form_label($form['nip']['label'],'nip', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['nip']['input']); ?>
            <?php echo form_error('nip', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['nama']['label'],'nama', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['nama']['input']); ?>
            <?php echo form_error('nama', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['alamat']['label'],'alamat', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['alamat']['input']); ?>
            <?php echo form_error('alamat', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['hp']['label'],'hp', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['hp']['input']); ?>
            <?php echo form_error('hp', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['jk']['label'],'jk', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_dropdown($form['jk']['input']); ?>
            <?php echo form_error('jk', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['tempat_lahir']['label'],'tempat_lahir', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['tempat_lahir']['input']); ?>
            <?php echo form_error('tempat_lahir', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['tgl_lahir']['label'],'tgl_lahir', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['tgl_lahir']['input']); ?>
            <?php echo form_error('tgl_lahir', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <?php echo form_label($form['email']['label'],'email', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['email']['input']); ?>
            <?php echo form_error('email', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
          <div class="col-md-10">
            <label class="text-danger">* : Wajib diisi.</label>
          </div>
        </div>
      </div>

      <div class="card-footer">
          <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit'));?>
          <?php echo anchor(base_url('profile/profil_dosen'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning'));?>
        
      </div>

        <?php echo form_close();?>
    </div>
  </div>
</div>
