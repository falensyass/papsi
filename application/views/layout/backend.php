<!DOCTYPE html>
<html lang="id">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title><?php echo show($title) .' | ' . $this->config->item('app_title'); ?></title>
    <style type="text/css">
      @media (max-width: 991.98px){
        .view-full{
          display:none !important;
        }
        .view-small{
          display:block !important;
        }
      }
    </style>
    <?php foreach ($list_css as $key_css => $url_css) : ?>
    <link href="<?php echo $url_css; ?>" rel="stylesheet">
    <?php endforeach; ?>
  </head>
  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    <header class="app-header navbar">
      <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url(); ?>">
        <img class="navbar-brand-full" src="<?php echo base_url('assets/img/ITS.png'); ?>" width="45" height="45">
        <img class="navbar-brand-full" src="<?php echo base_url('assets/img/papsi.png'); ?>" width="89" height="40">
      </a>
      <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <strong class="view-full" style="display: inline-block;"><?php show($this->ion_auth->user()->row()->first_name); ?></strong>

            <div class="img-avatar" style="display: inline-block; margin-bottom: -12px; width: 36px; height: 36px; background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url(<?php echo base_url('assets/img/avatars/user.jpg'); ?>);">
            </div>

          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-header text-center">
            <strong class="view-full">User Login</strong>
            <strong class="view-small" style="display: none">
            <?php show($this->ion_auth->user()->row()->first_name); ?></strong>
            </div>
            <?php if($this->ion_auth_acl->has_permission('pengaturan-profile-update')): ?>
            <a class="dropdown-item" href="<?php echo base_url('profile'); ?>">
              <i class="fa fa-user-circle-o "></i> Profil
            </a>
            <?php endif; ?>
            <a class="dropdown-item" href="<?php echo base_url('auth/change-password'); ?>">
              <i class="fa fa-lock"></i> Ganti Password
            </a>
            <a class="dropdown-item" href="<?php echo base_url('auth/logout'); ?>">
              <i class="fa fa-sign-out"></i> Keluar
            </a>
          </div>
        </li>
      </ul>
    </header>
    <div class="app-body">
      <div class="sidebar">
        <nav class="sidebar-nav">
          <ul class="nav">
            
            <li class="nav-title">Menu Utama</li>

            <?php foreach ($dashboard_menu as $title => $menu) : ?>

              <?php 
                  $cek_permission = FALSE;
                  foreach ($menu['permission'] as $key) {
                      $cek_permission = $cek_permission || $this->ion_auth_acl->has_permission($key);
                  }
                  if ($cek_permission) : 
                ?>

            
                <li class="nav-item nav-dropdown <?php echo !empty($menu['submenu']) ? expand_menu($menu['submenu']) : ''; ?>">
                  
                  <a class="nav-link <?php echo !empty($menu['submenu']) ? 'nav-dropdown-toggle': '' ; ?>" href="<?php echo !empty($menu['url']) ? $menu['url'] : '#' ; ?>">
                    <i class="<?php echo $menu['icon']; ?>"></i> <?php echo $title; ?>
                  </a>

                  <?php if (!empty($menu['submenu'])) : ?>

                      <ul class="nav-dropdown-items">
                        
                        <?php foreach ($menu['submenu'] as $submenu_title => $submenu) : ?>
                            <li class="nav-item">
                              <a class="nav-link <?php echo !empty($submenu['url']) ? active_menu($submenu['url']) : '' ; ?>" href="<?php echo $submenu['url']; ?>">
                                <i class="<?php echo $submenu['icon']; ?>"></i> <?php echo $submenu_title; ?>
                              </a>
                            </li>
                        <?php endforeach; ?>

                      </ul>

                <?php endif; ?>

                </li>
              <?php endif; ?>
            <?php endforeach; ?>

          </ul>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
      </div>
      <main class="main">
        <!-- Breadcrumb-->
        <ol class="breadcrumb">
        <?php
          if (isset($breadcrumbs)) :
            foreach ($breadcrumbs as $key_bread => $breadcrumb) :
        ?>

          <?php
            end($breadcrumbs);
            if ($key_bread == key($breadcrumbs)) :
          ?>
            <li class="breadcrumb-item">
              <?php echo $breadcrumb['title'] ?>
            </li>
        <?php else: ?>
            <li class="breadcrumb-item">
              <a href="<?php echo $breadcrumb['url']; ?>"><?php echo $breadcrumb['title'] ?></a>
            </li>
          <?php
              endif;
            endforeach;
          endif;
        ?>
          &nbsp;
          <!-- Breadcrumb Menu-->
        </ol>
        <div class="container-fluid">
          <div class="animated fadeIn">
            <?php echo $content; ?>
          </div>
        </div>
      </main>
      <aside class="aside-menu">
        <!-- Tab panes-->
      </aside>
    </div>
    <footer class="app-footer">
      <div>
        SIM Akademik
        <span>&copy; 2020 <a href="http://sekedar-tutorial.blogspot.com">PAPSI ITS</a></span>
      </div>
    </footer>
    
    <script type="text/javascript">
      var base_url = '<?php echo base_url(); ?>';
    </script>

    <!-- generate js yang akan di load di halaman backend -->
    <?php foreach ($list_js as $key_js => $url_js) : ?>
    <script src="<?php echo $url_js; ?>"></script>
    <?php endforeach; ?>

  </body>
</html>
