<div class="row">
  <div class="col-md-12">

  <?php echo show_alert($this->session->flashdata()); ?>
  </div>
</div>

<div class="row">
  <div class="col-md-12">

    <?php if (!empty($message_success)) : ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $message_success; ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <?php endif; ?>

<!-- data tabel -->
    
      <div class="card">
      <div class="card-header">
        <i class="nav-icon icon-badge"></i> Data <?php echo show($title); ?>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-condensed table-datatable">
                <thead>
                  <tr>
                      <th style="width: 10px;">No</th>
                      <th>Kelas</th>
                      <th style="width: 10px;">Pilihan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach($list_data as $data): ?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php show($data->kelas); ?></td>
                    <td class="text-center">

                        <div class="dropdown dropleft">
                          <button class="btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-menu"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="<?php echo base_url('laporan/transkrip/jadwal/'.$data->id); ?>"><i class="fa fa-eye"></i> Detail</a>
                          </div>
                        </div>
                        </div>
                    </td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- data table -->
  </div>
</div>
