<div class="row">
  <div class="col-md-12">

  <?php echo show_alert($this->session->flashdata()); ?>
  </div>
</div>

<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Transkrip</title>
    <style>
    .line-title{
      border: 0;
      border-style: inset;
      border-top: 1px solid #000;
    }
  </style>
  </head>
  <body>
    <div class="text-center" align="center">
        <img width="300px" src="assets\img\papsi.png">
    </div>
    <br>
    <table style="width: 100%">
      <tr>
        <td align="center">
          <span style="font-weight: bold; font-size: 20px">
            TRANSKRIP NILAI
          </span>
        </td>
      </tr>
    </table>

    <hr class="line-title">

    <table style="width: 50%">
      <tr>
        <td style="font-size: 15px">Kelas</td>
        <td style="font-size: 15px">: <?php show($list_matkul['kelas'])?></td>
      </tr>
      <tr>
        <td style="font-size: 15px">Mata Kuliah</td>
        <td style="font-size: 15px">: <?php show($list_matkul['nama_mk'])?></td>
      </tr>
      <tr>
        <td style="font-size: 15px">Nama Dosen</td>
        <td style="font-size: 15px">: <?php show($list_matkul['nama_dsn'])?></td>
      </tr>
      <tr>
        <td style="font-size: 15px">SKS</td>
        <td style="font-size: 15px">: <?php show($list_matkul['sks'])?></td>
      </tr>
      <tr>
        <td style="font-size: 15px">Tahun Ajaran</td>
        <td style="font-size: 15px">: <?php show($list_matkul['tahun_ajaran'])?></td>
      </tr>
    </table>
    <br>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td>No</td>
          <td>NRP</td>
          <td>Nama</td>
          <td>Nilai Tugas</td>
          <td>Nilai UTS</td>
          <td>Nilai UAS</td>
          <td>Nilai Total</td>
          <td>Nilai Huruf</td>
        </tr>
      </thead>
      <?php
        $no=1; 
        foreach($list_data as $val):
          echo "<tr>".
                "<td>".$no++."</td>".
                "<td>".$val['nrp']."</td>".
                "<td>".$val['nama']."</td>".
                "<td>".$val['tugas']."</td>".
                "<td>".$val['uts']."</td>".
                "<td>".$val['uas']."</td>".
                "<td>".$val['total']."</td>".
                "<td>".$val['huruf']."</td>".
                "</tr>";
        endforeach;
      ?>
    </table>
    <br>
    <table align="right" style="width: 50%">
        <tr>
          <td align="center">Mengetahui,</td>
        </tr>
        <tr>
          <td align="center">Ketua PAPSI ITS</td>
        </tr>
      </table>
      <br style="line-height: 10">
      <table align="right" style="width: 50%">
        <tr>
          <td align="center">Dr. Tahiyatul Asfihani, S.Si, M.Si</td>
        </tr>
        <tr>
          <td align="center">19870728 201404 2 001</td>
        </tr>
      </table>
  </body>
</html>
