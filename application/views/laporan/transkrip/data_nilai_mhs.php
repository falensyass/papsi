<div class="row">
  <div class="col-md-12">

  <?php echo show_alert($this->session->flashdata()); ?>
  </div>
</div>

<div class="row">
  <div class="col-md-12">

    <?php if (!empty($message_success)) : ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $message_success; ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <?php endif; ?>

<!-- data tabel -->
      <div class="card">
      <div class="card-header">
        <i class="nav-icon icon-chart"></i> Data <?php echo show($title); ?>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-condensed table-datatable">
                <thead>
                  <tr>
                      <th style="width: 10px;">No</th>
                      <th>Nama Mata Kuliah</th>
                      <th>Nilai Huruf</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach($list_data as $row): ?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <!-- <td><?php echo $row->id_nilai; ?></td> -->
                    <td><?php echo $row['nama_mk']; ?></td>
                    <td><?php echo $row['nilai_huruf']; ?></td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- data table -->
  </div>
</div>
