<div class="row">
  <div class="col-md-12">

    <?php if (!empty($message_success)) : ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $message_success; ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <?php endif; ?>

<!-- form -->
        <div class="card">
      <?php echo form_open('', array('class' => 'form-horizontal')); ?>
      <div class="card-header">
        <i class="nav-icon icon-flag"></i> <?php echo $title ?>
      </div>
      <div class="card-body">
        
        <div class="form-group row">
          <label class="control-label col-md-2">Kelas</label>
          <div class="col-md-10">
            <select class="form-control app-select2" name="kelas">
              <option value="">-- Pilih Kelas --</option>
              <option value="Kelas Pagi">Kelas Pagi</option>
              <option value="Kelas Siang">Kelas Siang</option>
              <option value="Kelas Sore">Kelas Sore</option>
            </select>
             <?php echo form_error('kelas', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

       
      </div>

      <div class="card-footer">
          <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Submit', 'class' => 'btn btn-primary', 'type' => 'submit'));?>
        
      </div>

        <?php echo form_close();?>
    </div>

<!-- data tabel -->
    <?php if(!empty($list_data)) : ?>
      <div class="card">
      <div class="card-header">
        <i class="nav-icon icon-people"></i> Data <?php echo show($title); ?>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-condensed table-datatable">
                <thead>
                  <tr>
                      <th style="width: 10px;">No</th>
                      <th>Hari</th>
                      <th>Kelas</th>
                      <th>Jam Masuk</th>
                      <th>Jam Pulang</th>
                      <th>Mata Kuliah</th>
                      <th>Dosen Pengajar</th>
                      <th style="width: 10px;">Pilihan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach($list_data as $data): ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <!-- <td><?php echo $row->id_nilai; ?></td> -->
                    <td><?php show($data->hari); ?></td>
                    <td><?php show($data->kelas); ?></td>
                    <td><?php show($data->jam_masuk); ?></td>
                    <td><?php show($data->jam_pulang); ?></td>
                    <td><?php show($data->matkul); ?></td>
                    <td><?php show($data->nama_dsn); ?></td>
                    <td class="text-center">

                        <div class="dropdown dropleft">
                          <button class="btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-menu"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="<?php echo base_url('laporan/transkrip/detail/'.$data->id); ?>"><i class="fa fa-eye"></i> Detail</a>
                            <a class="dropdown-item" href="<?php echo base_url('laporan/transkrip/print/'.$data->id); ?>"><i class="fa fa-print"></i> Print</a>
                          </div>
                        </div>
                        </div>
                    </td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>
    <!-- data table -->
  </div>
</div>
