<div class="row">
  <div class="col-md-12">

  <?php echo show_alert($this->session->flashdata()); ?>
  </div>
</div>

<!DOCTYPE html>
<html>
<head>
	<title>Transkrip Nilai</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<style>
		.line-title{
			border: 0;
			border-style: inset;
			border-top: 1px solid #000;
		}
	</style>
</head>
<body>
	<div class="text-center" align="center">
        <img width="300px" src="assets\img\papsi.png">
    </div>
    <br>
    <table style="width: 100%">
    	<tr>
    		<td align="center">
			    <span style="font-weight: bold; font-size: 25px">
			    	TRANSKRIP NILAI
			    </span>
    		</td>
    	</tr>
    </table>

    <hr class="line-title">

	<table style="width: 100%">
		<tr>
			<td>Nama</td>
			<td>: <?php show($list_mhs['nama'])?></td>
		</tr>
		<tr>
          <td>NRP</td>
          <td>: <?php show($list_mhs['nrp'])?></td>
        </tr>
        <tr>
          <td>Tempat / Tgl Lahir</td>
          <td>: <?php show($list_mhs['tempat_lahir'])?> / <?php echo output_date($list_mhs['tgl_lahir']) ?></td>
        </tr>
        <tr>
          <td>Tahun Masuk</td>
          <td>: <?php show($list_mhs['tahun_masuk'])?></td>
        </tr>
        <tr>
          <td>Tahun Lulus</td>
          <td>: <?php show($list_mhs['tahun_keluar'])?></td>
        </tr>
        <tr>
          <td>Indeks Prestasi Kumulatif (IPK)</td>
          <td>: <?php show($list_mhs['ipk'])?></td>
        </tr>
        <tr>
          <td>Predikat</td>
          <td>: <?php show($list_mhs['predikat'])?></td>
        </tr>
        <tr>
          <td>Judul Proyek Akhir</td>
          <td>: <?php show($list_mhs['judul_TA'])?></td>
        </tr>
	</table>
	<br>
	<table class="table table-bordered">
      <thead>
        <tr>
          <td>No</td>
          <td>Nama Matakuliah</td>
          <td>SKS</td>
          <td>Nilai Huruf</td>
        </tr>
      </thead>
      <tbody>
      <?php
        $no=1; 
        foreach($list_data as $val):
          echo "<tr>".
                "<td>".$no++."</td>".
                "<td>".$val['nama_mk']."</td>".
                "<td>".$val['sks']."</td>".
                "<td>".$val['nilai_huruf']."</td>".
                "</tr>";
        endforeach;
      ?>
      <tr>
      <td><?php echo $no++; ?></td>
      <td><?php show('Tugas Akhir')?></td>
      <td><?php show('6')?></td>
      <td><?php show($list_mhs['nilai_huruf'])?></td>
      </tr>
      </tbody>
      </table>
      <br>
      <table align="right" style="width: 50%">
      	<tr>
      		<td align="center">Mengetahui,</td>
      	</tr>
      	<tr>
      		<td align="center">Ketua PAPSI ITS</td>
      	</tr>
      </table>
      <br style="line-height: 10">
      <table align="right" style="width: 50%">
      	<tr>
      		<td align="center">Dr. Tahiyatul Asfihani, S.Si, M.Si</td>
      	</tr>
      	<tr>
      		<td align="center">19870728 201404 2 001</td>
      	</tr>
      </table>
</body>
</html>