<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('active_menu'))
{
    /**
     * menghasilkan class css untuk mengaktifkan menu navigasi jika controller sedang diakses
     * @param  string $controller   nama controller menu navigasi
     * @param  string $active_class nama class css yang akan di-return oleh fungsi ini
     * @return string               mengembalikan nama class css active jika controller sedang diakses, kosong jika controller tidak aktif
     */
    function active_menu($url, $active_class = 'active')
    {
        $current_url = current_url();

        if (strpos($current_url, $url) !== false) {
            return $active_class;
        }
        
        return;
    }
}

if ( ! function_exists('expand_menu'))
{
    /**
     * menghasilkan class css untuk mengaktifkan menu parent jika submenu di dalamnya sedang aktif
     * @param  array $list_submenu     array nama controller submenu
     * @param  string $active_class    nama class css yang ingin direturn oleh fungsi ini
     * @return string                  nama class css untuk mengaktifkan parent menu jika ada submenu yang aktif
     */
    function expand_menu($list_submenu, $active_class = 'open')
    {
        foreach ($list_submenu as $keysubmenu => $submenu) {
            $active_menu = active_menu(strtolower($submenu['url']));
            if (!empty($active_menu)) {
                return $active_class;
            }
        }

        return '';
    }
}