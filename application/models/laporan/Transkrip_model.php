<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transkrip_model extends CI_Model
{
    private $table = 'nilai';
    private $join = 'jadwal_kuliah';
    private $join2 = 'mahasiswa';
    private $join3 = 'matakuliah';
    private $join4 = 'dosen';
    private $join5 = 'kelas';
    private $join6 = 'kerja_praktik';
    private $join7 = 'anggota';

    public function get_data_nilai($id)
    {
      $this->db->select('nilai.id, nrp, mahasiswa.nama, nilai_tugas, nilai_uts, nilai_uas, mahasiswa.id as id_mahasiswa');
      $this->db->where('mahasiswa.aktif', 1);
      $this->db->where('kelas.aktif', 1);
      $this->db->where('jadwal_kuliah.id', $id);
      $this->db->join($this->join2, 'mahasiswa.id = nilai.id_mhs','left');
      $this->db->join($this->join, 'jadwal_kuliah.id = nilai.id_jadwal');
      $this->db->join($this->join3, 'matakuliah.id = jadwal_kuliah.id_matkul');
      $this->db->join($this->join5, 'kelas.id = jadwal_kuliah.kelas');
        return $this->db->get($this->table);
    }

    public function get_data()
    {

      $this->db->select('nilai.id, nilai_tugas, nilai_uts, nilai_uas, jadwal_kuliah.nama as nama_mk, mahasiswa.nama as nama_mhs');
      $this->db->where('jadwal_kuliah.aktif', 1);
      $this->db->where('mahasiswa.aktif', 1);
      $this->db->where('kelas.aktif', 1);
      $this->db->join($this->join, 'jadwal_kuliah.id = nilai.id_matkul');
      $this->db->join($this->join, 'jadwal_kuliah.id = nilai.id_matkul');
      $this->db->join($this->join5, 'kelas.id = jadwal_kuliah.kelas');

      return $this->db->get($this->table);
    }

    public function get_data_matkul()
    {
        return $this->db->get_where($this->join3, array('aktif' => 1));
    }

    public function get_matkul($id)
    { 
      $this->db->select('matakuliah.id, matakuliah.nama as nama_mk, sks, semester, tahun_ajaran, kelas.kelas, dosen.nama as nama_dsn');
      $this->db->where('matakuliah.aktif', 1);
      $this->db->where('dosen.aktif', 1);
      $this->db->where('kelas.aktif', 1);
      $this->db->where('jadwal_kuliah.id', $id);
      $this->db->join($this->join4, 'jadwal_kuliah.id_dosen = dosen.id');
      $this->db->join($this->join3, 'jadwal_kuliah.id_matkul = matakuliah.id');
      $this->db->join($this->join5, 'kelas.id = jadwal_kuliah.kelas');

      return $this->db->get($this->join);
    }
    public function get_mhs($id)
    {
      $this->db->select('mahasiswa.id,nrp,nama');
      $this->db->where('mahasiswa.aktif', 1);
      $this->db->where('kelas.aktif', 1);
      $this->db->where('jadwal_kuliah.id', $id);
      $this->db->join($this->join5, 'kelas.id = mahasiswa.kelas');
      $this->db->join($this->join, 'kelas.id = jadwal_kuliah.kelas');
      return $this->db->get($this->join2);
    }

    public function get_nilai_mhs($id)
    {
      $this->db->select('nilai.id, nilai_tugas, nilai_uts, nilai_uas, sks, dosen.nama as nama_dsn, matakuliah.nama as nama_mk');
      $this->db->where('mahasiswa.aktif', 1);
      $this->db->where('kelas.aktif', 1);
      $this->db->where('dosen.aktif', 1);
      $this->db->where('mahasiswa.nrp', $id);
      $this->db->join($this->join2, 'mahasiswa.id = nilai.id_mhs','left');
      $this->db->join($this->join, 'jadwal_kuliah.id = nilai.id_jadwal');
      $this->db->join($this->join3, 'matakuliah.id = jadwal_kuliah.id_matkul');
      $this->db->join($this->join4, 'dosen.id = jadwal_kuliah.id_dosen');
      $this->db->join($this->join5, 'kelas.id = jadwal_kuliah.kelas');
        return $this->db->get($this->table);
    }

    public function get_mhs_kelas($id)
    {
      $this->db->select('mahasiswa.id, nrp, nama');
      $this->db->where('mahasiswa.aktif', 1);
      $this->db->where('kelas.aktif', 1);
      $this->db->where('kelas.id', $id);
      $this->db->join($this->join5, 'kelas.id = mahasiswa.kelas');
      return $this->db->get($this->join2);
    }

    public function get_jadwal($id)
    {
      $this->db->select('jadwal_kuliah.id, kelas.kelas, id_matkul, matakuliah.nama as matkul, hari, jam_masuk, jam_pulang, id_dosen, dosen.nama as nama_dsn');
      $this->db->where('kelas.id', $id);
      $this->db->join($this->join3, 'jadwal_kuliah.id_matkul = matakuliah.id');
      $this->db->join($this->join4, 'jadwal_kuliah.id_dosen = dosen.id');
      $this->db->join($this->join5, 'jadwal_kuliah.kelas = kelas.id');
      return $this->db->get($this->join);
    }

    public function get_all_mhs(){
      return $this->db->get_where($this->join2, array('aktif' => 1));
    }

    public function get_all(){
      $this->db->select('nilai.id, nrp, mahasiswa.nama, nilai_tugas, nilai_uts, nilai_uas, mahasiswa.id as id_mahasiswa');
      $this->db->where('mahasiswa.aktif', 1);
      $this->db->where('matakuliah.id ');
      $this->db->where('jadwal_kuliah.kelas');
      $this->db->join($this->join2, 'mahasiswa.id = nilai.id_mhs','left');
      $this->db->join($this->join, 'jadwal_kuliah.id = nilai.id_jadwal');
      $this->db->join($this->join3, 'matakuliah.id = jadwal_kuliah.id_matkul');
        return $this->db->get($this->table);
    }
    
    public function get_kelas(){
      return $this->db->get_where($this->join5, array('aktif' => 1));
    }

    public function get_data_mahasiswa($nrp)
    {

      $this->db->select('jadwal_kuliah.id, hari, jam_masuk, jam_pulang, kelas.kelas, matakuliah.nama as nama_mk, dosen.nama as nama_dsn');
      $this->db->where('matakuliah.aktif', 1);
      $this->db->where('dosen.aktif', 1);
      $this->db->where('kelas.aktif',1);
      $this->db->where('mahasiswa.nrp',$nrp);
      $this->db->join($this->join, 'jadwal_kuliah.id = nilai.id_jadwal');
      $this->db->join($this->join3, 'matakuliah.id = jadwal_kuliah.id_matkul');
      $this->db->join($this->join4, 'dosen.id = jadwal_kuliah.id_dosen');
      $this->db->join($this->join5, 'kelas.id = jadwal_kuliah.kelas');
      $this->db->join($this->join2, 'kelas.id = mahasiswa.kelas');

      return $this->db->get($this->table);
    }

    public function get_data_mhs($nrp)
    {

      $this->db->select('nama, nrp, tempat_lahir, tgl_lahir, tahun_masuk, tahun_keluar, judul_TA, nilai');
      $this->db->where('mahasiswa.nrp',$nrp);
      $this->db->join($this->join7, 'mahasiswa.id = anggota.id_mhs');
      $this->db->join($this->join6, 'kerja_praktik.id = anggota.id_kp');

      return $this->db->get($this->join2);
    }
}