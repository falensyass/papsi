<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile_model extends CI_Model
{
    private $table1 = 'dosen';
    private $table2 = 'mahasiswa';

    public function get_data()
    {
        $this->db->where('aktif', 1);
        return $this->db->get($this->table);
    }

    public function get_data_by_id($id)
    {
      return $this->db->get_where($this->table, array('id' => $id))->row_array();
    }

    public function get_data_mahasiswa_by_id($id)
    {
      return $this->db->get_where($this->table2, array('nrp' => $id))->row_array();
    }

    public function get_data_dosen_by_id($id)
    {
      return $this->db->get_where($this->table1, array('nip' => $id))->row_array();
    }

    public function update_mahasiswa($id, $data)
    {
      $this->db->where('nrp', $id);
      $this->db->update($this->table2, $data);

      return $this->db->affected_rows();
    }

    public function update_dosen($id, $data)
    {
      $this->db->where('nip', $id);
      $this->db->update($this->table1, $data);

      return $this->db->affected_rows();
    }

    public function update($id, $data)
    {
      $this->db->where('id', $id);
      $this->db->update($this->table, $data);

      return $this->db->affected_rows();
    }
}