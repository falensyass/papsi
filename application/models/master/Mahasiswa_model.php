<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa_model extends CI_Model
{
    private $table = 'mahasiswa';
    private $join = 'kelas';

    public function get_data()
    {
        $this->db->where('aktif', 1);
        return $this->db->get($this->table);
    }

    public function insert($data)
    {
      return $this->db->insert($this->table, $data);
    }

    public function get_data_by_id($id)
    {
      return $this->db->get_where($this->table, array('id' => $id))->row_array();
    }

    public function update($id, $data)
    {
      $this->db->where('id', $id);
      $this->db->update($this->table, $data);

      return $this->db->affected_rows();
    }

    public function delete($id)
    {
      $this->db->set('aktif', 0);
      $this->db->where('id', $id);
      $this->db->update($this->table);

      return $this->db->affected_rows();
    }

    public function get_all_kelas(){
      return $this->db->get_where($this->join, array('aktif' => 1));
    }

    public function check_user_mahasiswa_by_id($id)
    {
      return $this->db->get_where('users', array('username' => $id));
    }

    public function check_mahasiswa_by_id($id)
    {
      return $this->db->get_where($this->table, array('nrp' => $id));
    }

    public function update_user_mahasiswa($username, $email, $firstname)
    {
        $this->db->set('email', $email);
        $this->db->set('first_name', $firstname);
        $this->db->where('username', $username);
        $this->db->update('users');
    }
}