<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_model extends CI_Model
{
    private $table = 'karyawan';

    public function __construct()
    {
      $this->load->library('datatables');
    }

    public function get_data_karyawan()
    {
        $this->datatables
             ->select("*")
             ->from($this->table);

        $this->db->order_by('id', 'DESC');

        return $this->datatables->generate();
    }
}