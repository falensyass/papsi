<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
    private $table = 'jadwal_kuliah';
    private $join = 'matakuliah';
    private $join2 = 'dosen';
    private $join3 = 'kelas';
    private $join4 = 'mahasiswa';

    public function get_data_mahasiswa($nrp)
    {
      $this->db->select('jadwal_kuliah.id, hari, jam_masuk, jam_pulang, kelas.kelas, matakuliah.nama as nama_mk, dosen.nama as nama_dsn');
      $this->db->where('matakuliah.aktif', 1);
      $this->db->where('dosen.aktif', 1);
      $this->db->where('kelas.aktif',1);
      $this->db->where('mahasiswa.nrp',$nrp);
      $this->db->join($this->join, 'matakuliah.id = jadwal_kuliah.id_matkul');
      $this->db->join($this->join2, 'dosen.id = jadwal_kuliah.id_dosen');
      $this->db->join($this->join3, 'kelas.id = jadwal_kuliah.kelas');
      $this->db->join($this->join4, 'kelas.id = mahasiswa.kelas');

      return $this->db->get($this->table);
    } 

    public function get_data_dosen($nip)
    {
      $this->db->select('jadwal_kuliah.id, hari, jam_masuk, jam_pulang, kelas.kelas, matakuliah.nama as nama_mk, dosen.nama as nama_dsn');
      $this->db->where('matakuliah.aktif', 1);
      $this->db->where('dosen.aktif', 1);
      $this->db->where('kelas.aktif',1);
      $this->db->where('dosen.nip',$nip);
      $this->db->join($this->join, 'matakuliah.id = jadwal_kuliah.id_matkul');
      $this->db->join($this->join2, 'dosen.id = jadwal_kuliah.id_dosen');
      $this->db->join($this->join3, 'kelas.id = jadwal_kuliah.kelas');

      return $this->db->get($this->table);
    }
}
