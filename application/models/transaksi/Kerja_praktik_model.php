<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kerja_praktik_model extends CI_Model
{
    private $table = 'kerja_praktik';
    private $join  = 'mahasiswa';
    private $join2 = 'dosen';
    private $join3 = 'anggota';


    public function get_data()
    {
      $this->db->select("kerja_praktik.id, judul_TA, perusahaan, dosen_pembimbing.nama as nama_pembimbing, dosen_penguji.nama as nama_penguji");
      $this->db->where('kerja_praktik.aktif', 1);
      $this->db->where('dosen_penguji.aktif', 1);
      $this->db->where('dosen_pembimbing.aktif', 1);
      $this->db->join($this->join2." as dosen_pembimbing", "dosen_pembimbing.id=kerja_praktik.id_dosen_pembimbing");
      $this->db->join($this->join2." as dosen_penguji", "dosen_penguji.id=kerja_praktik.id_dosen_penguji");
      return $this->db->get($this->table);
    }

    public function get_data_dosen($nip)
    {
      $this->db->select("kerja_praktik.id, judul_TA, perusahaan, dosen_pembimbing.nama as nama_pembimbing, dosen_penguji.nama as nama_penguji");
      $this->db->where('kerja_praktik.aktif', 1);
      $this->db->where('dosen_penguji.aktif', 1);
      $this->db->where('dosen_pembimbing.aktif', 1);
      $this->db->where('(dosen_pembimbing.nip = '.$nip.' OR '.'dosen_penguji.nip = '.$nip.')');
      $this->db->join($this->join2." as dosen_pembimbing", "dosen_pembimbing.id=kerja_praktik.id_dosen_pembimbing");
      $this->db->join($this->join2." as dosen_penguji", "dosen_penguji.id=kerja_praktik.id_dosen_penguji");
      return $this->db->get($this->table);
    }

    public function get_data_mahasiswa($nrp)
    {
      $this->db->select("kerja_praktik.id, judul_TA, perusahaan, dosen_pembimbing.nama as nama_pembimbing, dosen_penguji.nama as nama_penguji");
      $this->db->where('kerja_praktik.aktif', 1);
      $this->db->where('dosen_penguji.aktif', 1);
      $this->db->where('dosen_pembimbing.aktif', 1);
      $this->db->where('mahasiswa.aktif', 1);
      $this->db->where('mahasiswa.nrp', $nrp);
      $this->db->join($this->join2." as dosen_pembimbing", "dosen_pembimbing.id=kerja_praktik.id_dosen_pembimbing");
      $this->db->join($this->join2." as dosen_penguji", "dosen_penguji.id=kerja_praktik.id_dosen_penguji");
      $this->db->join($this->join3, "anggota.id_kp = kerja_praktik.id");
      $this->db->join($this->join, "mahasiswa.id = anggota.id_mhs");
      return $this->db->get($this->table);
    }

    public function get_mahasiswa()
    {
      $this->db->select("mahasiswa.id, nrp, nama, id_kp, anggota.id, nilai");
      $this->db->where('mahasiswa.aktif', 1);
      $this->db->join($this->join, "mahasiswa.id = anggota.id_mhs");
      return $this->db->get($this->join3);
    }

    public function insert($data)
    {
      return $this->db->insert($this->table, $data);
    }

    public function get_all_dosen(){
      return $this->db->get_where($this->join2, array('aktif' => 1));
    }

    public function get_all_mahasiswa()
    {
      $this->db->where('kerja_praktik.aktif', 1);
      $this->db->join($this->table, 'kerja_praktik.id = anggota.id_kp');
      $anggota = $this->db->get($this->join3)->result();
      foreach($anggota as $val){
        $mhs[]= $val->id_mhs;
      }

      if (!empty($mhs)){
        $this->db->where_not_in('id', $mhs);
      }
      $this->db->where('aktif', 1);
      return $this->db->get($this->join);
    }

    public function get_all_anggota($id)
    {
      $this->db->select("mahasiswa.id, nrp, nama, id_kp, anggota.id as id_anggota, nilai");
      $this->db->where('mahasiswa.aktif', 1);
      $this->db->where('anggota.id_kp', $id);
      $this->db->join($this->join, "mahasiswa.id = anggota.id_mhs");
      return $this->db->get($this->join3);
    }


    public function get_data_by_id($id)
    {
      return $this->db->get_where($this->table, array('id' => $id))->row_array();
    }

    public function update($id, $data)
    {
      $this->db->where('id', $id);
      $this->db->update($this->table, $data);

      return $this->db->affected_rows();
    }

    public function delete($id)
    {
      $this->db->set('aktif', 0);
      $this->db->where('id', $id);
      $this->db->update($this->table);

      return $this->db->affected_rows();
    }

    public function insert_anggota($data, $id)
    {
      $list_data_anggota = array();
      foreach($data['id_mhs'] as $key => $value){
        $list_data_anggota = array(
          'id_kp' => $id,
          'id_mhs' => $value
        );
        $insert = $this->db->insert($this->join3, $list_data_anggota);
      }

      return $insert;
    }

     public function insert_nilai($data)
    {
      $list_data_anggota = array();
      foreach($data['nilai'] as $key => $value){
        // $list_data_anggota = array(
        //   'id' => $data['id'][$key],
        //   'nilai' => $value
        // );
        $this->db->set('nilai',$value);
        $this->db->where('id', $data['id'][$key]);
        $insert = $this->db->update($this->join3);
      }

      return $insert;
    }
}