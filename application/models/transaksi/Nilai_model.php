<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai_model extends CI_Model
{
  private $table = 'nilai';
  private $join = 'jadwal_kuliah';
  private $join2 = 'mahasiswa';
  private $join3 = 'matakuliah';
  private $join4 = 'dosen';
  private $join5 = 'kelas';

    public function get_data_nilai($id)
    {
      $this->db->select('nilai.id, nrp, mahasiswa.nama, nilai_tugas, nilai_uts, nilai_uas, mahasiswa.id as id_mahasiswa, jadwal_kuliah.id as id_jadwal');
      $this->db->where('mahasiswa.aktif', 1);
      $this->db->where('kelas.aktif', 1);
      $this->db->where('jadwal_kuliah.id', $id);
      $this->db->join($this->join2, 'mahasiswa.id = nilai.id_mhs','left');
      $this->db->join($this->join, 'jadwal_kuliah.id = nilai.id_jadwal');
      $this->db->join($this->join3, 'matakuliah.id = jadwal_kuliah.id_matkul');
      $this->db->join($this->join5, 'kelas.id = jadwal_kuliah.kelas');
        return $this->db->get($this->table);
    }

    public function get_data()
    {

      $this->db->select('nilai.id, nilai_tugas, nilai_uts, nilai_uas, jadwal_kuliah.nama as nama_mk, mahasiswa.nama as nama_mhs');
      $this->db->where('jadwal_kuliah.aktif', 1);
      $this->db->where('mahasiswa.aktif', 1);
      $this->db->where('kelas.aktif', 1);
      $this->db->join($this->join, 'jadwal_kuliah.id = nilai.id_matkul');
      $this->db->join($this->join, 'jadwal_kuliah.id = nilai.id_matkul');
      $this->db->join($this->join5, 'kelas.id = jadwal_kuliah.kelas');

      return $this->db->get($this->table);
    }

    public function get_data_matkul()
    {
        return $this->db->get($this->join3);
    }

    public function cek($id_jadwal, $id_mhs){
      return $this->db->get_where($this->table, array('id_jadwal'=>$id_jadwal,'id_mhs'=>$id_mhs));
    }

    public function get_mhs($id)
    {
      $this->db->select('mahasiswa.id,nrp,nama');
      $this->db->where('mahasiswa.aktif', 1);
      $this->db->where('kelas.aktif', 1);
      $this->db->where('jadwal_kuliah.id', $id);
      $this->db->join($this->join5, 'kelas.id = mahasiswa.kelas');
      $this->db->join($this->join, 'kelas.id = jadwal_kuliah.kelas');
      return $this->db->get($this->join2);
    }

    public function get_nilai_mhs($id)
    {
      $this->db->select('nilai.id, nilai_tugas, nilai_uts, nilai_uas, dosen.nama as nama_dsn, matakuliah.nama as nama_mk');
      $this->db->where('mahasiswa.aktif', 1);
      $this->db->where('kelas.aktif', 1);
      $this->db->where('dosen.aktif', 1);
      $this->db->where('mahasiswa.nrp', $id);
      $this->db->join($this->join2, 'mahasiswa.id = nilai.id_mhs','left');
      $this->db->join($this->join, 'jadwal_kuliah.id = nilai.id_jadwal');
      $this->db->join($this->join3, 'matakuliah.id = jadwal_kuliah.id_matkul');
      $this->db->join($this->join4, 'dosen.id = jadwal_kuliah.id_dosen');
      $this->db->join($this->join5, 'kelas.id = jadwal_kuliah.kelas');
        return $this->db->get($this->table);
    }

    public function get_jadwal($id)
    {
      $this->db->select('jadwal_kuliah.id, kelas.kelas, id_matkul, matakuliah.nama as matkul, hari, jam_masuk, jam_pulang, id_dosen, dosen.nama as nama_dsn');
      $this->db->where('kelas.id', $id);
      $this->db->join($this->join3, 'jadwal_kuliah.id_matkul = matakuliah.id');
      $this->db->join($this->join4, 'jadwal_kuliah.id_dosen = dosen.id');
      $this->db->join($this->join5, 'jadwal_kuliah.kelas = kelas.id');
      return $this->db->get($this->join);
    }

    public function get_jadwal_dosen($nip)
    {
      $this->db->select('jadwal_kuliah.id, kelas.kelas, id_matkul, matakuliah.nama as matkul, hari, jam_masuk, jam_pulang, id_dosen, dosen.nama as nama_dsn');
      $this->db->where('dosen.nip', $nip);
      $this->db->join($this->join3, 'jadwal_kuliah.id_matkul = matakuliah.id');
      $this->db->join($this->join4, 'jadwal_kuliah.id_dosen = dosen.id');
      $this->db->join($this->join5, 'jadwal_kuliah.kelas = kelas.id');
      return $this->db->get($this->join);
    }

    public function get_all_mhs(){
      return $this->db->get_where($this->join2, array('aktif' => 1));
    }

    public function get_data_by_id($id)
    {
      return $this->db->get_where($this->table, array('nilai.id' => $id))->row_array();
    }

    public function update($id, $data)
    {
      $this->db->where('nilai.id', $id);
      $this->db->update($this->table, $data);

      return $this->db->affected_rows();
    }

    public function insert_nilai($data)
    {
      foreach ($data['id_mhs'] as $key => $value) {
        if($data['nilai_uts'][$key] != '' && $data['nilai_tugas'][$key] != '' && $data['nilai_uas'][$key] != ''){
          $table = array(
            'id_jadwal' => $data['id_jadwal'][$key],
            'id_mhs' => $value,
            'nilai_uts' => $data['nilai_uts'][$key],
            'nilai_tugas' => $data['nilai_tugas'][$key],
            'nilai_uas' => $data['nilai_uas'][$key]
          );
          $cek = $this->db->get_where($this->table, array('id_jadwal'=>$table['id_jadwal'],'id_mhs'=>$table['id_mhs']));

          if($cek->num_rows() > 0 ){
            $this->db->where('nilai.id_mhs', $table['id_mhs']);
            $this->db->where('nilai.id_jadwal', $table['id_jadwal']);
            $this->db->update($this->table, $table);

            $insert = $this->db->affected_rows();
          }
          else{
            $insert = $this->db->insert($this->table, $table);
          }
        }
      }
      return $insert;
    }

    public function delete($id)
    {
      $this->db->where('id', $id);
      $this->db->delete($this->table);
    }

    public function get_kelas(){
      return $this->db->get_where($this->join5, array('aktif' => 1));
    }
}