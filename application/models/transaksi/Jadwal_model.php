<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_model extends CI_Model
{
    private $table = 'jadwal_kuliah';
    private $join = 'matakuliah';
    private $join2 = 'dosen';
    private $join3 = 'kelas';
    private $join4 = 'mahasiswa';

    public function get_data()
    {
      $this->db->select('jadwal_kuliah.id, hari, jam_masuk, jam_pulang, kelas.kelas, matakuliah.nama as nama_mk, dosen.nama as nama_dsn');
      $this->db->where('matakuliah.aktif', 1);
      $this->db->where('dosen.aktif', 1);
      $this->db->where('kelas.aktif',1);
      $this->db->join($this->join, 'matakuliah.id = jadwal_kuliah.id_matkul');
      $this->db->join($this->join2, 'dosen.id = jadwal_kuliah.id_dosen');
      $this->db->join($this->join3, 'kelas.id = jadwal_kuliah.kelas');

      return $this->db->get($this->table);
    }

    public function get_data_dosen($nip)
    {

      $this->db->select('jadwal_kuliah.id, hari, jam_masuk, jam_pulang, kelas.kelas, matakuliah.nama as nama_mk, dosen.nama as nama_dsn');
      $this->db->where('matakuliah.aktif', 1);
      $this->db->where('dosen.aktif', 1);
      $this->db->where('kelas.aktif',1);
      $this->db->where('dosen.nip',$nip);
      $this->db->join($this->join, 'matakuliah.id = jadwal_kuliah.id_matkul');
      $this->db->join($this->join2, 'dosen.id = jadwal_kuliah.id_dosen');
      $this->db->join($this->join3, 'kelas.id = jadwal_kuliah.kelas');

      return $this->db->get($this->table);
    }

    public function get_data_mahasiswa($nrp)
    {

      $this->db->select('jadwal_kuliah.id, hari, jam_masuk, jam_pulang, kelas.kelas, matakuliah.nama as nama_mk, dosen.nama as nama_dsn');
      $this->db->where('matakuliah.aktif', 1);
      $this->db->where('dosen.aktif', 1);
      $this->db->where('kelas.aktif',1);
      $this->db->where('mahasiswa.nrp',$nrp);
      $this->db->join($this->join, 'matakuliah.id = jadwal_kuliah.id_matkul');
      $this->db->join($this->join2, 'dosen.id = jadwal_kuliah.id_dosen');
      $this->db->join($this->join3, 'kelas.id = jadwal_kuliah.kelas');
      $this->db->join($this->join4, 'kelas.id = mahasiswa.kelas');

      return $this->db->get($this->table);
    }

    public function insert($data)
    {
      return $this->db->insert($this->table, $data);
    }

    public function get_data_by_id($id)
    {
      return $this->db->get_where($this->table, array('id' => $id))->row_array();
    }

    public function update($id, $data)
    {
      $this->db->where('id', $id);
      $this->db->update($this->table, $data);

      return $this->db->affected_rows();
    }
    
    public function get_all_matkul(){
      return $this->db->get_where($this->join, array('aktif' => 1));
    }

    public function get_all_kelas(){
      return $this->db->get_where($this->join3, array('aktif' => 1));
    }
    
    public function get_all_dosen(){
      return $this->db->get_where($this->join2, array('aktif' => 1));
    }

    public function delete($id)
    {
      $this->db->where('id', $id);
      $this->db->delete($this->table);
    }
    
}
