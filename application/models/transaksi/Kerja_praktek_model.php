<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kerja_praktek_model extends CI_Model
{
    private $table = 'kerja_praktek';

    public function __construct()
    {
      $this->load->library('datatables');
    }

    public function get_data_kp()
    {
        return $this->db->get($this->table)->result();
    }
}