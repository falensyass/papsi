$(document).ready(function(){
    //fungsi untuk mengaktifkan datatable
    $(".table-datatable").DataTable({
        "language": {
            "url": base_url+"assets/vendors/datatables/indonesia.json"
        },
        "columnDefs": [{ "orderable": false, "targets": -1 }]
    });

    //fungsi untuk menampilkan konfirmasi hapus data
    $(".btn-delete").on("click", function(e){
        e.preventDefault();
        var urlhapus = $(this).attr("href");

        Swal.fire({
          title: 'Konfirmasi',
          text: "Apakah Anda yakin akan menghapus data?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#f86c6b',
          cancelButtonColor: '#20a8d8',
          confirmButtonText: 'Hapus',
          cancelButtonText: 'Tidak'
        }).then((result) => {
          if (result.value) {
            window.location.href = urlhapus;
          }
        });
    });

    setTimeout(function() {
      $(".alert").hide();
    }, 4000)
    
     // init select2
     $(".app-select2").select2({
      theme: 'bootstrap4',
    });

    //init datepicker
    $('.app-datepicker').flatpickr({
      locale: 'id',
      dateFormat: 'Y-m-d',
      altInput: true,
      altFormat: "d F Y",
    });

    //init datepicker
    $('.app-timepicker').flatpickr({
      locale: 'id',
      enableTime: true,
      enableSeconds: true,
      altInput: true,
      altFormat: "H:i:S",
      noCalendar: true,
      dateFormat: "H:i:S",
      time_24hr: true
    });
});