$('.karyawan-datatable').DataTable({
  'responsive': true,
  "processing": true, //Feature control the processing indicator.
  "serverSide": true, //Feature control DataTables' server-side processing mode.
  "order": [], //Initial no order.
  // Load data for the table's content from an Ajax source
  "ajax": {
      "url": base_url + 'master/karyawan/get_data_karyawan',
      "type": "POST"
  },
  //Set column definition initialisation properties.
  "columns": [
      {"data": "KodeProgram", className: 'text-center',},
      {"data": "NamaProgram"},
      {"data": "Keterangan"},
      {
        "data": "IDProgram",
        "render": function(data, type, row, meta) {
          var link = '<div class="btn-group"><a class="btn btn-sm btn-warning" href="'+base_url+'master/program/edit/'+data+'"><i class="fa fa-edit"></i></a> ';
          link    += '<a class="btn btn-sm btn-danger btn-hapus"  href="'+base_url+'master/program/delete/'+data+'"><i class="fa fa-trash"></i></a></div>';
          return link;
        },
        className: 'text-center',
      },
  ],
  'language' : datatable_indonesia(),
});