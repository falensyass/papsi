# CodeStarter
CodeStarter adalah Codeigniter starter untuk memudahkan dan mempercepat proses pengembangan aplikasi berbasis web.

# CONFIG FILE
buatlah file baru di dalam folder **config** :

- buat file baru dengan nama **config.php** dengan meng-copy dari file **config.example.php** lalu sesuaikan **config.php** dengan pengaturan Anda.
- buat file baru dengan nama **database.php** dengan meng-copy dari file **database.example.php** lalu sesuaikan **database.php** dengan pengaturan Anda.

# IMPORT DATABASE
buat database baru lalu import file database **codestarter.sql** yang ada di folder DB.

# MENU NAVIGASI
sesuaikan menu navigasi dengan mengedit file **config/navigation.php**

# USERNAME DAN PASSWORD
- username : admin
- password : password