-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 02, 2020 at 12:34 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simpapsi`
--

-- --------------------------------------------------------

--
-- Table structure for table `anggota`
--

CREATE TABLE `anggota` (
  `id` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `id_kp` int(11) NOT NULL,
  `nilai` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota`
--

INSERT INTO `anggota` (`id`, `id_mhs`, `id_kp`, `nilai`) VALUES
(1, 1, 1, ''),
(2, 1, 2, ''),
(3, 2, 2, ''),
(4, 3, 2, '85');

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `id` int(11) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `hp` varchar(15) NOT NULL,
  `jk` varchar(10) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `aktif` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`id`, `nip`, `nama`, `alamat`, `hp`, `jk`, `tempat_lahir`, `tgl_lahir`, `email`, `aktif`) VALUES
(1, '195911211989031001', 'Drs. I Gst Ngr Rai Usadha, M.Si', 'Surabaya', '08236545966', 'Laki-Laki', '', '1995-11-15', 'rusadha@gmail.com', 1),
(2, '198707282014042001', 'Dr. Tahiyatul Asfihani, S.Si, M.Si', 'Gresik', '085612365478', 'Perempuan', '', '0000-00-00', 'hani@gmail.com', 1),
(3, '197202071997021001', 'Dr. Budi Setiyono, S.Si, MT', 'Surabaya', '082244159685', 'Laki-Laki', '', '0000-00-00', 'budi123@gmail.com', 1),
(4, '196904051994032003', 'Dr. Dwi Ratna Sulistyaningrum, S.Si, MT', 'Surabaya', '082336522541', 'Perempuan', '', '0000-00-00', 'ratnadwi@gmail.com', 1),
(5, '195911211989031003', 'Triani', 'Surabaya', '08236545966', 'Perempuan', 'Sidoarjo', '2020-08-11', 'tri@gmail.com', 1),
(6, '195911211989031001', 'Hani', 'Wonosari, RT 007 RW 002, Tarik, Sidoarjo', '08564525', 'Perempuan', 'Sidoarjo', '2020-08-24', 'fallensyass@gmail.com', 0),
(7, '195911211989031001', 'Bendung', 'Wonosari, RT 007 RW 002, Tarik, Sidoarjo', '08564525', 'Perempuan', 'Sidoarjo', '2020-08-24', 'fbidoira@gmail.com', 0),
(8, '195911211989031001', 'Bendung', 'Wonosari, RT 007 RW 002, Tarik, Sidoarjo', '08564525', 'Laki-Laki', 'Sidoarjo', '2020-08-24', 'masansaafif@gmail.com', 0),
(9, '195911211989031001	', 'Bendung', 'Wonosari, RT 007 RW 002, Tarik, Sidoarjo', '08564525', 'Laki-Laki', 'Sidoarjo', '2020-08-25', 'fallensyass@gmail.com', 0),
(10, '12364', 'Diki', 'Surabaya', '08564525', 'Laki-Laki', 'Sidoarjo', '2020-08-19', 'adikasurya02@gmail.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(4, 'members', 'General User'),
(5, 'dosen', 'Dosen'),
(6, 'mahasiswa', 'Mahasiswa');

-- --------------------------------------------------------

--
-- Table structure for table `groups_permissions`
--

CREATE TABLE `groups_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `perm_id` int(11) NOT NULL,
  `value` tinyint(4) DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups_permissions`
--

INSERT INTO `groups_permissions` (`id`, `group_id`, `perm_id`, `value`, `created_at`, `updated_at`) VALUES
(356, 6, 9, 1, 1598205673, 1598205673),
(357, 6, 16, 1, 1598205673, 1598205673),
(358, 6, 44, 1, 1598205673, 1598205673),
(359, 6, 13, 1, 1598205673, 1598205673),
(360, 6, 15, 1, 1598205673, 1598205673),
(361, 6, 14, 1, 1598205673, 1598205673),
(371, 5, 9, 1, 1598499294, 1598499294),
(372, 5, 44, 1, 1598499294, 1598499294),
(373, 5, 13, 1, 1598499294, 1598499294),
(374, 5, 43, 1, 1598499294, 1598499294),
(375, 5, 15, 1, 1598499294, 1598499294),
(376, 5, 29, 1, 1598499294, 1598499294),
(377, 5, 14, 1, 1598499294, 1598499294),
(378, 5, 30, 1, 1598499294, 1598499294),
(379, 1, 9, 1, 1598696689, 1598696689),
(380, 1, 17, 1, 1598696689, 1598696689),
(381, 1, 16, 1, 1598696689, 1598696689),
(382, 1, 20, 1, 1598696689, 1598696689),
(383, 1, 22, 1, 1598696689, 1598696689),
(384, 1, 10, 1, 1598696689, 1598696689),
(385, 1, 21, 1, 1598696689, 1598696689),
(386, 1, 39, 1, 1598696690, 1598696690),
(387, 1, 41, 1, 1598696690, 1598696690),
(388, 1, 38, 1, 1598696690, 1598696690),
(389, 1, 40, 1, 1598696690, 1598696690),
(390, 1, 23, 1, 1598696690, 1598696690),
(391, 1, 25, 1, 1598696690, 1598696690),
(392, 1, 11, 1, 1598696690, 1598696690),
(393, 1, 24, 1, 1598696690, 1598696690),
(394, 1, 35, 1, 1598696690, 1598696690),
(395, 1, 37, 1, 1598696690, 1598696690),
(396, 1, 12, 1, 1598696690, 1598696690),
(397, 1, 36, 1, 1598696690, 1598696690),
(398, 1, 4, 1, 1598696690, 1598696690),
(399, 1, 5, 1, 1598696690, 1598696690),
(400, 1, 19, 1, 1598696690, 1598696690),
(401, 1, 8, 1, 1598696690, 1598696690),
(402, 1, 6, 1, 1598696690, 1598696690),
(403, 1, 18, 1, 1598696690, 1598696690),
(404, 1, 26, 1, 1598696690, 1598696690),
(405, 1, 28, 1, 1598696690, 1598696690),
(406, 1, 13, 1, 1598696690, 1598696690),
(407, 1, 27, 1, 1598696690, 1598696690),
(408, 1, 42, 1, 1598696690, 1598696690),
(409, 1, 32, 1, 1598696691, 1598696691),
(410, 1, 34, 1, 1598696691, 1598696691),
(411, 1, 43, 1, 1598696691, 1598696691),
(412, 1, 15, 1, 1598696691, 1598696691),
(413, 1, 33, 1, 1598696691, 1598696691),
(414, 1, 29, 1, 1598696691, 1598696691),
(415, 1, 31, 1, 1598696691, 1598696691),
(416, 1, 14, 1, 1598696691, 1598696691),
(417, 1, 30, 1, 1598696691, 1598696691);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_kuliah`
--

CREATE TABLE `jadwal_kuliah` (
  `id` int(11) NOT NULL,
  `hari` varchar(7) NOT NULL,
  `jam_masuk` time NOT NULL,
  `jam_pulang` time NOT NULL,
  `kelas` int(11) NOT NULL,
  `id_matkul` int(11) NOT NULL,
  `id_dosen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal_kuliah`
--

INSERT INTO `jadwal_kuliah` (`id`, `hari`, `jam_masuk`, `jam_pulang`, `kelas`, `id_matkul`, `id_dosen`) VALUES
(1, 'Senin', '08:00:00', '10:00:00', 1, 1, 3),
(2, 'Selasa', '08:00:00', '10:00:00', 2, 1, 3),
(3, 'Rabu', '08:00:00', '10:00:00', 3, 1, 3),
(4, 'Selasa', '10:00:00', '12:00:00', 3, 1, 1),
(5, 'Selasa', '11:00:00', '12:00:00', 1, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id` int(11) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `aktif` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id`, `kelas`, `aktif`) VALUES
(1, 'Kelas Pagi', 1),
(2, 'Kelas Siang', 1),
(3, 'Kelas Sore', 1),
(4, 'Kelas Malam', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kerja_praktik`
--

CREATE TABLE `kerja_praktik` (
  `id` int(11) NOT NULL,
  `perusahaan` varchar(255) NOT NULL,
  `judul_TA` varchar(255) NOT NULL,
  `aktif` int(11) NOT NULL DEFAULT '1',
  `id_dosen_pembimbing` int(11) NOT NULL,
  `id_dosen_penguji` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kerja_praktik`
--

INSERT INTO `kerja_praktik` (`id`, `perusahaan`, `judul_TA`, `aktif`, `id_dosen_pembimbing`, `id_dosen_penguji`) VALUES
(1, 'SMKN 123 Sidoarjo', 'Sistem Jual Beli Barang Bekas', 0, 1, 1),
(2, 'PAPSI ITS', 'Sistem Informasi Manajemen Akademik PAPSI ITS', 1, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id` int(11) NOT NULL,
  `nrp` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `hp` varchar(15) NOT NULL,
  `jk` varchar(10) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `asal_sekolah` varchar(255) NOT NULL,
  `asal_kota` varchar(255) NOT NULL,
  `nama_ortu` varchar(255) NOT NULL,
  `alamat_ortu` varchar(255) NOT NULL,
  `hp_ortu` varchar(15) NOT NULL,
  `kelas` int(11) NOT NULL,
  `tahun_masuk` int(11) NOT NULL,
  `tahun_keluar` int(11) NOT NULL,
  `aktif` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `nrp`, `nama`, `alamat`, `hp`, `jk`, `tempat_lahir`, `tgl_lahir`, `email`, `asal_sekolah`, `asal_kota`, `nama_ortu`, `alamat_ortu`, `hp_ortu`, `kelas`, `tahun_masuk`, `tahun_keluar`, `aktif`) VALUES
(1, '8819300012', 'Moch. Rafi\'Uddin Afifurrochman', 'Kediri', '089630544033', 'Laki-Laki', 'Kediri', '2001-01-17', 'suhu@gmail.com', 'SMKN 1 Kediri', 'Kediri', 'Panoyo', 'Kediri', '082256326655', 1, 0, 0, 1),
(2, '8819300059', 'Falensya Sakaningrum Sofiaji', 'Sidoarjo', '082337740365', 'Perempuan', 'Krian', '2000-04-04', 'falensya@gmail.com', 'SMAN 1 Krian', 'Krian', 'Muji', 'Sidoarjo', '085869853666', 2, 2019, 2020, 1),
(3, '8819300071', 'Adika Surya Perdana', 'Sidoarjo', '087765336521', 'Laki-Laki', 'Sidoarjo', '2001-08-01', 'fbidoira@gmail.com', 'SMAN 1 Krian', 'Sidoarjo', 'Budi', 'Sidoarjo', '082263235632', 1, 2019, 2020, 1),
(4, '8819300023', 'Vicky Asbihan', 'Lamongan', '085698523652', 'Laki-Laki', 'Lamongan', '2001-08-08', 'vicky@gmail.com', 'SMAN 1 Lamongan', 'Lamongan', 'Budi', 'Lamongan', '082256932516', 1, 0, 0, 1),
(5, '8819300018', 'Alvin Putra Muliyanda', 'Sidoarjo', '082256325698', 'Laki-Laki', 'Sidoarjo', '2000-12-30', 'alvinputra999@gmail.com', 'SMAN 1 Krian', 'Sidoarjo', 'Mulyono', 'Sidoarjo', '085696320256', 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `matakuliah`
--

CREATE TABLE `matakuliah` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `sks` int(11) NOT NULL,
  `semester` varchar(10) NOT NULL,
  `tahun_ajaran` varchar(5) NOT NULL,
  `aktif` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `matakuliah`
--

INSERT INTO `matakuliah` (`id`, `nama`, `sks`, `semester`, `tahun_ajaran`, `aktif`) VALUES
(1, 'VB', 3, 'Ganjil', '2020', 1),
(2, 'Android', 3, 'Ganjil', '2020', 1),
(3, 'PHP', 3, 'Ganjil', '2020', 1),
(4, 'Microsoft Office', 3, 'Ganjil', '2020', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `id` int(11) NOT NULL,
  `nilai_tugas` int(11) NOT NULL,
  `nilai_uts` int(11) NOT NULL,
  `nilai_uas` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`id`, `nilai_tugas`, `nilai_uts`, `nilai_uas`, `id_mhs`, `id_jadwal`) VALUES
(1, 68, 78, 88, 1, 1),
(2, 78, 65, 70, 3, 1),
(4, 80, 34, 90, 1, 5),
(5, 52, 34, 52, 3, 5),
(6, 56, 80, 80, 4, 5),
(7, 56, 12, 52, 5, 5),
(9, 90, 80, 95, 4, 1),
(10, 89, 75, 98, 5, 1),
(11, 90, 80, 95, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `perm_key` varchar(50) NOT NULL,
  `perm_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `perm_key`, `perm_name`) VALUES
(4, 'pengaturan-hak_akses-create', 'Create Hak Akses'),
(5, 'pengaturan-hak_akses-delete', 'Delete Hak Akses'),
(6, 'pengaturan-pengguna-create', 'Create Pengguna'),
(7, 'master-anggota-create', 'Create Anggota'),
(8, 'pengaturan-hak_akses-update', 'Update Hak Akses'),
(9, 'dashboard-beranda-read', 'Read Dashboard'),
(10, 'master-dosen-read', 'Read Master Dosen'),
(11, 'master-mahasiswa-read', 'Read Master Mahasiswa'),
(12, 'master-matakuliah-read', 'Read Master Matakuliah'),
(13, 'transaksi-jadwal-read', 'Read Transaksi Jadwal'),
(14, 'transaksi-nilai-read', 'Read Transaksi Nilai'),
(15, 'transaksi-kerja_praktik-read', 'Read Transaksi Kerja Praktik'),
(16, 'laporan-transkrip-read', 'Read Laporan Transkrip'),
(17, 'laporan-sertifikat-read', 'Read Laporan Sertifikat'),
(18, 'pengaturan-pengguna-read', 'Read Pengguna'),
(19, 'pengaturan-hak_akses-read', 'Read Hak Akses'),
(20, 'master-dosen-create', 'Create Master Dosen'),
(21, 'master-dosen-update', 'Update Master Dosen'),
(22, 'master-dosen-delete', 'Delete Master Dosen'),
(23, 'master-mahasiswa-create', 'Create Master Mahasiswa'),
(24, 'master-mahasiswa-update', 'Update Master Mahasiswa'),
(25, 'master-mahasiswa-delete', 'Delete Master Mahasiswa'),
(26, 'transaksi-jadwal-create', 'Create Transaksi Jadwal'),
(27, 'transaksi-jadwal-update', 'Update Transaksi Jadwal'),
(28, 'transaksi-jadwal-delete', 'Delete Transaksi Jadwal'),
(29, 'transaksi-nilai-create', 'Create Transaksi Nilai'),
(30, 'transaksi-nilai-update', 'Update Transaksi Nilai'),
(31, 'transaksi-nilai-delete', 'Delete Transaksi Nilai'),
(32, 'transaksi-kerja_praktik-create', 'Create Transaksi Kerja Praktik'),
(33, 'transaksi-kerja_praktik-update', 'Update Transaksi Kerja Praktik'),
(34, 'transaksi-kerja_praktik-delete', 'Delete Transaksi Kerja Praktik'),
(35, 'master-matakuliah-create', 'Create Master Matakuliah'),
(36, 'master-matakuliah-update', 'Update Master Matakuliah'),
(37, 'master-matakuliah-delete', 'Delete Master Matakuliah'),
(38, 'master-kelas-read', 'Read Master Kelas'),
(39, 'master-kelas-create', 'Create Master Kelas'),
(40, 'master-kelas-update', 'Update Master Kelas'),
(41, 'master-kelas-delete', 'Delete Master Kelas'),
(42, 'transaksi-kerja_praktik-anggota', 'Create Anggota Kerja Praktik'),
(43, 'transaksi-kerja_praktik-nilai-create', 'Create Transaksi Kerja Praktik Nilai'),
(44, 'pengaturan-profile-update', 'Update Profile');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'admin', '$2y$12$poAjCwNFj7hUEeJI4HZrL.eu0gb/S6qGLVIjy4Ry4MtIQPfpAX/H6', 'andi.sholihins@gmail.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1598854436, 1, 'Admin', 'Keren', 'ADMIN', '081751509955'),
(2, '::1', 'tono', '$2y$10$Gh4a03rc5Bg6jbFVpdwA/e7W1ueTHgTKuiXe2rCw1Jtezm/KQ/dne', 'arnoldarmando07@gmail.com', NULL, NULL, 'c15e9e62ec1bd8096f09', '$2y$10$eV6C.7tBBxn1/HiQe1hYb.nyjWC9nuzZrjMqPoHU3BdGsxDvOOqmO', 1597824613, NULL, NULL, 1562561882, NULL, 1, 'Toni', 'Hartono', '', ''),
(3, '::1', 'john', '$2y$10$noNe0HyS/GIMWfI0NXONzeaHLnWxSDEtHG2sGVMxvBFQyxjCJEShe', 'john@example.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1562590867, NULL, 1, 'John', 'Doe', '', '0'),
(5, '::1', '114', '$2y$10$f/eVIRJyFkf.01rEJ1qGh.SPaYrS9liFQWh6vUHhCx.uJmJjYj.ei', 'adikasurya02@gmail.com', NULL, NULL, '4c458cf9c3b195391d9c', '$2y$10$Dr/mPEsGY1BQ/E54NNK/8..pbD.a/imUwYXk/XfezililKlcF5Uie', 1597824554, NULL, NULL, 1597226012, 1597227685, 1, 'Bendung', NULL, 'SISWA', NULL),
(6, '::1', '115', '$2y$10$y.mREPlxi1E3Ah04VESmGu8WrJGeU3WdOrBiXN/q8ZeCE96I4DxIC', 'falensyasofiaji@yahoo.co.id', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1597226012, NULL, 1, 'Saluran', NULL, 'SISWA', NULL),
(10, '::1', '0236589125', '$2y$10$aO7MqAXz7W3aFyohSXwt8.o1wVxU/hE0SMMqVHcidc/oTfdDBowD2', 'masansaafif@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1597226675, 1597226702, 1, 'Hani', NULL, 'DOSEN', NULL),
(12, '::1', '8819300012', '$2y$10$8MXb4TIzj7ZZ7w/TQvNOBeph3ztAqcV9lGU6Ea0kQlRs31k8rOKAK', 'suhu@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1597896700, 1598506480, 1, 'Moch. Rafi\'Uddin Afifurrochman', NULL, 'SISWA', NULL),
(13, '::1', '8819300059', '$2y$10$T6EM1yNqD1OD.RSbbl4VyuhKqJIbOcC1uFAadjmzSDgec.IFFQVwG', 'Tester@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1597896701, 1598845609, 1, 'Falensya Sakaningrum Sofiaji', NULL, 'SISWA', NULL),
(14, '::1', '8819300071', '$2y$10$K2/2yluyqQpsNcPCn1WM1.Qs8uznBJqs6FHN5wNRJThzxxKUwDB.G', 'fbidoira@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1597896701, 1598852825, 1, 'Adika Surya Perdana', NULL, 'SISWA', NULL),
(15, '::1', '8819300023', '$2y$10$rsjDHziQkdAO0C1USyd62.8nKPDz2yKi4f1nyGfKWNUo4u5RRJ9mu', 'vicky@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1597896701, 1597991863, 1, 'Vicky Asbihan', NULL, 'SISWA', NULL),
(16, '::1', '8819300018', '$2y$10$gAG24ixxG2ICFad..BPuH.OwGcduY9Vo7yWeYG7JHUorBPvU/Qjf6', 'alvinputra999@gmail.com', NULL, NULL, '9504c5e5f6efc6aa0c98', NULL, NULL, NULL, NULL, 1597896702, 1597900070, 1, 'Alvin Putra Muliyanda', NULL, 'SISWA', NULL),
(17, '::1', '195911211989031001', '$2y$10$gfEZMFQumWRsHVaxLAudn.BpnNNrC5RjxzTCEtbomDrWpQsKtxgOu', 'rusadha@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1597986292, 1598851230, 1, 'Drs. I Gst Ngr Rai Usadha, M.Si', NULL, 'DOSEN', NULL),
(18, '::1', '198707282014042001', '$2y$10$uCcqs05c0zHuadcTjmimMeGr2P2ipmVOt8e5gLTkJKMAtbnEy9.nO', 'hani@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1597986293, 1597990671, 1, 'Dr. Tahiyatul Asfihani, S.Si, M.Si', NULL, 'DOSEN', NULL),
(19, '::1', '197202071997021001', '$2y$10$CQ0aoBxl2UsuL5wCi/EgweT1sEhmfjmHuKGKVGfc4altAz35GoJ6C', 'budi123@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1597986293, 1598202882, 1, 'Dr. Budi Setiyono, S.Si, MT', NULL, 'DOSEN', NULL),
(20, '::1', '196904051994032003', '$2y$10$8dyMqrMMEjYnJiKoLo5X8ufeAM5X8ODog47saehEdHfgqtC85.HKm', 'ratnadwi@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1597986293, NULL, 1, 'Dr. Dwi Ratna Sulistyaningrum, S.Si, MT', NULL, 'DOSEN', NULL),
(21, '::1', '195911211989031003', '$2y$10$.UMeTDsjK7T5hWW3grJohef2MmQfJ5.EayFRYkJpAtzC5wjbY2ca2', 'tri@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1598505516, NULL, 1, 'Triani', NULL, 'DOSEN', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(56, 1, 1),
(52, 2, 4),
(54, 3, 4),
(59, 5, 6),
(60, 6, 6),
(61, 10, 5),
(62, 12, 6),
(63, 13, 6),
(64, 14, 6),
(65, 15, 6),
(66, 16, 6),
(67, 17, 5),
(68, 18, 5),
(69, 19, 5),
(70, 20, 5),
(71, 21, 5);

-- --------------------------------------------------------

--
-- Table structure for table `users_permissions`
--

CREATE TABLE `users_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `perm_id` int(11) NOT NULL,
  `value` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups_permissions`
--
ALTER TABLE `groups_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roleID_2` (`group_id`,`perm_id`);

--
-- Indexes for table `jadwal_kuliah`
--
ALTER TABLE `jadwal_kuliah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kerja_praktik`
--
ALTER TABLE `kerja_praktik`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matakuliah`
--
ALTER TABLE `matakuliah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permKey` (`perm_key`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_username` (`username`),
  ADD UNIQUE KEY `uc_email` (`email`),
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `users_permissions`
--
ALTER TABLE `users_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userID` (`user_id`,`perm_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anggota`
--
ALTER TABLE `anggota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `dosen`
--
ALTER TABLE `dosen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `groups_permissions`
--
ALTER TABLE `groups_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=418;

--
-- AUTO_INCREMENT for table `jadwal_kuliah`
--
ALTER TABLE `jadwal_kuliah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kerja_praktik`
--
ALTER TABLE `kerja_praktik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `matakuliah`
--
ALTER TABLE `matakuliah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `users_permissions`
--
ALTER TABLE `users_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
